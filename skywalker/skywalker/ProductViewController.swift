//
//  ProductViewController.swift
//  skywalker
//
//  Created by Sabari on 2/13/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,CZPickerViewDelegate, CZPickerViewDataSource,UITextFieldDelegate{
    
    var jsonProductData=NSArray()
    var jsonLocationandProductData=NSArray()
    var jsonProductClassData=NSArray()
    var jsonProductFeatureData=NSArray()
    var getproductbySection = [[String]]()
    var getproductValuebySection = [[Int]]()
    var getproductIdbySection = [[Int]]()
    var getPlayerproductIdbySection = [[Int]]()
    var productVariableIdValue = [AnyObject]()
    var assignProductValue = [Int]()
    var getProductValue = [String]()
    var getProductId = [Int]()
    var getPlayerProductId = [Int]()
    var checkStatus = Int()
    var urlString = String()
    var checkStatusCount = Int()
    var checkIntialState = Int()
    var picker = CZPickerView()
    var productClassPicker = CZPickerView()
    var featurePicker = CZPickerView()
    var getProductFeatureId = [Int]()
    var getProductFromPickerId = Int()
    var productNames = String()
    var urlStrings = ApiUrl()
    var segment = ViewController()
    var simulateValue = String()
    var getSteps = String()
    
    @IBOutlet weak var productName: UITextField!
    @IBOutlet weak var productFeature: UITextField!
    @IBOutlet weak var productList: UITextField!
    var getPlayerProductValus = NSArray()
    @IBOutlet weak var newProductView: UIView!
    @IBOutlet weak var productCollection: UICollectionView!
    @IBOutlet weak var productTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String
        newProductView.isHidden = true;
        checkStatusCount = 0
        checkIntialState = 0
        productTable.backgroundColor=UIColor.clear
        //recognize when user touch outside
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AllocationViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        productTable.addGestureRecognizer(tapGesture)
        //add corner radius for both collectionview and table view
        productCollection.layer.borderColor = UIColor.white.cgColor
        productCollection.layer.borderWidth = 0.7
        productCollection.layer.cornerRadius = 5
        productCollection.layer.masksToBounds = true
        
        //add corner radius for save and commit button
        self.commitButton.layer.cornerRadius=5
        self.commitButton.layer.masksToBounds=true
        self.saveButton.layer.cornerRadius=5
        self.saveButton.layer.masksToBounds=true
        //add delegate for textfield
        productList.delegate=self
        productFeature.delegate=self
        //update detail if user select and save location
        NotificationCenter.default.addObserver(self, selector: #selector(ProductViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProductViewController.segmentChange(notification:)), name: Notification.Name("RedirectSegment"), object: nil)
        
        
        for i in 0..<SegmentViewController.RealTime.realtimeTabArray.count
        {
            if(SegmentViewController.RealTime.realtimeTabArray[i] == "PRODUCT")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
                getSteps = "PRODUCT"
                break
            }
            else
            {
                self.saveButton.isUserInteractionEnabled = false
                self.commitButton.isUserInteractionEnabled = false
                getSteps = ""
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkIsCommit()
    }
    func methodOfReceivedNotification(notification: Notification){
        self.getProductClass()
        self.getProductListData()
        self.getLocationAndProductData()    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return jsonProductData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProductCollectionViewCell
        let product = (jsonProductData[indexPath.item] as AnyObject).value(forKey: "Name")
        let PlayerProductName = (jsonProductData[indexPath.item] as AnyObject).value(forKey: "PlayerProductName")
        if(product is NSNull)
        {
            cell.productTitle!.text = PlayerProductName as? String
        }
        else
        {
            cell.productTitle!.text = product as? String
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(getSteps == "PRODUCT" || simulateValue != "RealTime")
        {
            if(checkStatus == 2)
            {
                let product = (jsonProductData[indexPath.item] as AnyObject).value(forKey: "Name")
                let PlayerProductName = (jsonProductData[indexPath.item] as AnyObject).value(forKey: "PlayerProductName")
                if(product is NSNull)
                {
                    productNames = PlayerProductName as! String
                }
                else
                {
                    productNames = product as! String
                }
                let id = (jsonProductData[indexPath.item] as AnyObject).value(forKey: "Id")
                let playerId = (jsonProductData[indexPath.item] as AnyObject).value(forKey: "PlayerProductId")
                getProductValue.append(productNames)
                getProductId.append((id as? Int)!)
                getPlayerProductId.append((playerId as? Int)!)
                picker = CZPickerView(headerTitle: "countryName", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
                picker.delegate = self
                picker.dataSource = self
                picker.needFooterView = false
                picker.show()
            }
        }
        else
        {
            Util.showNotification(title: "",subTitle: urlStrings.bannerMessage ,color: Util.BannerColors.blue)
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return getPlayerProductValus.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        return getproductbySection[section].count
        
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let locationName = (self.jsonLocationandProductData[section] as AnyObject).value(forKey: "LocationName")
        return locationName as? String
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.productTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor=UIColor.clear
        cell.textLabel?.textColor=UIColor.white
        let product = cell.contentView.viewWithTag(1) as! UILabel
        let value = cell.contentView.viewWithTag(2) as! UILabel
        product.text = getproductbySection[indexPath.section][indexPath.row]
        value.text = String(getproductValuebySection[indexPath.section][indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    /*!
     *@brief get all location data from server and reload the tableview to update the data
     */
    func getProductListData()
    {
        let urlValue = URL(string: urlStrings.getProductList)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonProductData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        DispatchQueue.main.async(){
                            self.productCollection.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    
    /*!
     *@brief get Location And Product Data from server and reload the tableview to update the data
     */
    func getLocationAndProductData()
    {
        let urlValue = URL(string: urlStrings.getPlayerLocationandProductId)!//stepid=2
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonLocationandProductData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        print(self.jsonLocationandProductData)
                        self.getPlayerProductValus = self.jsonLocationandProductData.value(forKey: "playerValueProduct")as! NSArray
                        for j in 0..<self.getPlayerProductValus.count
                        {
                            let value = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "IsSelected") as! NSArray
                            let state = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "CheckState") as! NSArray
                            var productValue = [String]()
                            var productId = [Int]()
                            var playerProductId = [Int]()
                            var productValues = [Int]()
                            for i in 0..<value.count
                            {
                                if(value[i] as! Bool)
                                {
                                    let productName = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "ProductName") as! NSArray
                                    let PlayerProductName = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "PlayerProductName") as! NSArray
                                    let productid = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "ProductId") as! NSArray
                                    let playerProductid = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "PlayerProductId") as! NSArray
                                    let value = (self.getPlayerProductValus[j] as AnyObject).value(forKey: "Value") as! NSArray
                                    if(productName[i] is NSNull)
                                    {
                                        productValue.append(PlayerProductName[i] as! String)
                                    }
                                    else
                                    {
                                        productValue.append(productName[i] as! String)
                                    }
                                    productId.append(productid[i] as! Int)
                                    playerProductId.append(playerProductid[i] as! Int)
                                    productValues.append(value[i] as! Int)
                                    //for new user Is selected is 0 this loop is not execute for new user
                                    self.checkIntialState = 1
                                    
                                }
                                if(value[i] as! Bool == true)
                                {
                                    if(state[i] as! Int != 0)
                                    {
                                        self.checkStatusCount = 1;
                                        self.checkStatus = state[i] as! Int
                                    }
                                }
                            }
                            self.getproductbySection.append(productValue)
                            self.getproductValuebySection.append(productValues)
                            self.getproductIdbySection.append(productId)
                            self.getPlayerproductIdbySection.append(playerProductId)
                        }
                        if(self.checkStatusCount == 0)
                        {
                            self.checkStatus = 0
                        }
                        //In First time(new user) check State is 0.
                        //if checkstatus = 2 save button intraction isenable
                        //set checkStatus =2
                        //now savebutton will be enable at new user
                        if(self.checkIntialState == 0)
                        {
                            self.checkStatus = 2
                        }
                        if(self.checkStatus == 0)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = false
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 2)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.saveButton.layer.borderColor = UIColor.white.cgColor
                                    self.saveButton.layer.borderWidth=1
                                    self.commitButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = true
                                    self.commitButton.isUserInteractionEnabled=false
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 1)
                        {
                            DispatchQueue.main.async()
                                {
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.isHidden=true
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Revoke",for: .normal)
                            }
                            
                        }
                        DispatchQueue.main.async(){
                            // self.locationCollection.reloadData()
                            self.productTable.reloadData()
                            self.stepCheck()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    //call picker view when user tap on textfield.
    //based on textfield name picker will display
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField == productList)
        {
            productClassPicker = CZPickerView(headerTitle: "Select Product", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
            productClassPicker.delegate = self
            productClassPicker.dataSource = self
            productClassPicker.needFooterView = false
            productClassPicker.show()
            return false
        }
        else if(textField == productFeature)
        {
            featurePicker = CZPickerView(headerTitle: "Select Feature", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
            featurePicker.delegate = self
            featurePicker.dataSource = self
            featurePicker.needFooterView = false
            featurePicker.show()
            return false
            
        }
        else
        {
            return true
        }
    }
    
    //show product list in picker based on pickerview name
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        if(pickerView == picker)
        {
            return self.jsonLocationandProductData.count
        }
        else if (pickerView == productClassPicker)
        {
            return self.jsonProductClassData.count
        }
        else if (pickerView == featurePicker)
        {
            return self.jsonProductFeatureData.count
        }
        else
        {
            return 0
        }
    }
    
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        if(pickerView == picker)
        {
            let location = (self.jsonLocationandProductData[row] as AnyObject).value(forKey: "LocationName")
            return location as! String!
        }
        else if (pickerView == productClassPicker)
        {
            let productName = (self.jsonProductClassData[row] as AnyObject).value(forKey: "Name")
            if productName is NSNull
            {
                return "dummy"
            }
            else
            {
                return productName as! String!
            }
        }
        else if (pickerView == featurePicker)
        {
            let productFeatureName = (self.jsonProductFeatureData[row] as AnyObject).value(forKey: "Name")
            return productFeatureName as! String!
        }
        else
        {
            return nil
        }
    }
    
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int){
        if(pickerView == picker)
        {
            getproductbySection[row].append(getProductValue[0])
            getproductIdbySection[row].append(getProductId[0])
            getPlayerproductIdbySection[row].append(getPlayerProductId[0])
            for j in 0..<self.getPlayerProductValus.count
            {
                getproductbySection[j] = uniq(source: getproductbySection[j]) as Array
                getproductIdbySection[j] = uniq(source: getproductIdbySection[j]) as Array
                getPlayerproductIdbySection[j] = uniq(source: getPlayerproductIdbySection[j]) as Array
            }
            let alertController = UIAlertController(title: productNames, message: "", preferredStyle: .alert)
            
            let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
                alert -> Void in
                
                let firstTextField = alertController.textFields![0] as UITextField
                let valueText = Int(firstTextField.text!)
                self.getproductValuebySection[row].append(valueText!)
                self.productTable .reloadData()
                self.getProductValue.removeAll()
                self.getProductId.removeAll()
                self.getPlayerProductId.removeAll()
            })
            
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Target Value"
            }
            
            alertController.addAction(saveAction)
            // alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        else if (pickerView == productClassPicker)
        {
            let value =  (self.jsonProductClassData[row] as AnyObject).value(forKey: "Name")!
            let getProductId =  (self.jsonProductClassData[row] as AnyObject).value(forKey: "Id")!
            productList.text = value as? String
            getProductFromPickerId = getProductId as! Int
            getProductFeature(productId: getProductId as! Int)
        }
        else if (pickerView == featurePicker)
        {
            let value =  (self.jsonProductFeatureData[row] as AnyObject).value(forKey: "Name")!
            let getProductFeatureValue =  (self.jsonProductFeatureData[row] as AnyObject).value(forKey: "Id")!
            
            productFeature.text = value as? String
            let id = getProductFeatureValue as! Int
            getProductFeatureId.append(id)
        }
        
    }
    
    func czpickerViewDidClickCancelButton(_ pickerView: CZPickerView!) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemsAtRows rows: [AnyObject]!) {
        if(pickerView == picker)
        {
            for row in rows {
                if let row = row as? Int {
                    print((self.jsonLocationandProductData[row] as AnyObject).value(forKey: "LocationName")!)
                }
            }
        }
    }
    
    //remove duplicate element from array function
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    
    @IBAction func saveButton(_ sender: Any) {
        urlString = urlStrings.savePlayerProduct
        saveProduct()
        //send notification when user in realtime simulation
        if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Product Saved"])
        }
        
    }
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var commitButton: UIButton!
    @IBAction func commitButton(_ sender: Any) {
        
        if(self.checkStatus == 1)
        {
            urlString = urlStrings.revokePlayerProduct
            saveProduct()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Product Revoked"])
            }
        }
        else  if(self.checkStatus == 0)
        {
            urlString = urlStrings.submitPlayerProduct
            saveProduct()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Product Commited"])
            }
        }
    }
    func saveProduct()
    {
        var getelement = [String : Any]()
        var productJsonarr = [Any]()
        print(getproductbySection)
        print(getproductIdbySection)
        print(getPlayerproductIdbySection)
        
        for i in 0...self.jsonLocationandProductData.count-1
        {
            for j in 0..<getproductIdbySection[i].count
            {
                
                let gameVariable = ["ProductId":getproductIdbySection[i][j],"playerProductId":0, "Value":getproductValuebySection[i][j]] as [String : Any?]
                productVariableIdValue.append(gameVariable as AnyObject)
            }
            getelement = ["stepId": 2, "SaveLocationId":(self.jsonLocationandProductData[i] as AnyObject).value(forKey: "locationId")!,"playervaluePricingProduct":productVariableIdValue] as [String : Any]
            productVariableIdValue.removeAll()
            productJsonarr.append(getelement)
            
        }
        let jsonData = try! JSONSerialization.data(withJSONObject: productJsonarr, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        print(jsonString)
        let urlValue = URL(string: urlString)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let PostResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(PostResponse)
                        if(self.checkStatus == 0)
                        {
                            let userId = UserDefaults.standard
                            let segVal = userId.value(forKey: "segmentVal") as! Int
                            userId.set(segVal+1, forKey: "segmentVal")
                            userId.synchronize()
                            let sendSegmentValue = ["SegmentValue":userId.value(forKey: "segmentVal")]
                            print(sendSegmentValue)
                            NotificationCenter.default.post(name: Notification.Name("RedirectSegment"), object:nil,userInfo:sendSegmentValue)
                        }
                        //after save checkStatus set to 0
                        if(self.checkStatus == 2)
                        {
                            
                            if(PostResponse == "true")
                            {
                                self.checkStatus=0
                                self.getLocationAndProductData()
                            }
                        }
                        else
                        {
                            self.getLocationAndProductData()
                        }
                    }
                    
                }
                
        })
        
        task.resume()
        
    }
    //hide collection view and table view
    @IBAction func addProduct(_ sender: Any)
    {
        self.productCollection.isHidden = true
        self.productTable.isHidden = true
        
        //add zoom animation when touch plus symbol
        //scall uiview from 0.1 to 1.0
        self.newProductView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        UIView.animate(withDuration:0.5, animations: {() -> Void in
            self.newProductView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        }, completion: {(_ finished: Bool) -> Void in
            print(" Animation complet Block")
        })
        self.newProductView.isHidden = false
    }
    
    //hide new produt view when cancel
    //show all apart from new productView
    @IBAction func newProductClose(_ sender: Any) {
        self.newProductView.isHidden = true
        self.productCollection.isHidden = false
        self.productTable.isHidden = false
    }
    @IBAction func newProductOk(_ sender: Any) {
        print(getProductFeatureId)
        let passElement = ["PlayerProductName": "\(productName.text!)", "PlayerProductId":getProductFromPickerId,"ProductFeatureId":getProductFeatureId] as [String : Any]
        let jsonData = try! JSONSerialization.data(withJSONObject: passElement, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        print(jsonString)
        let urlValue = URL(string: urlStrings.saveProductFeature)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let PostResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(PostResponse)
                        self.viewDidLoad()
                    }
                }
        })
        task.resume()
        
    }
    
    //get productlist from server
    func getProductClass()
    {
        let urlValue = URL(string: urlStrings.getProducts )!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    self.jsonProductClassData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSArray
                    print(self.jsonProductClassData)
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        task.resume()
    }
    
    //get getProductFeature from server
    func getProductFeature(productId : Int)
    {
        let url = urlStrings.getFeaturesList_productid
        let urlValue = URL(string:"\(url)\(productId)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    self.jsonProductFeatureData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSArray
                    print(self.jsonProductClassData)
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        task.resume()
    }
    func hideKeyboard() {
        productTable.endEditing(true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        animateCell(cell: cell)
    }
    
    func animateCell(cell: UITableViewCell) {
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.fromValue = 200
        cell.layer.cornerRadius = 0
        animation.toValue = 0
        animation.duration = 1
        cell.layer.add(animation, forKey: animation.keyPath)
    }
    func checkIsCommit()
    {
        let url = urlStrings.isCommit
        let urlValue = URL(string: "\(url)\(2)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                    print(postResponse)
                    if(postResponse == "true")
                    {
                        let userId = UserDefaults.standard
                        let segVal = userId.value(forKey: "segmentVal") as! Int
                        userId.set(segVal+1, forKey: "segmentVal")
                        userId.synchronize()
                        self.getProductClass()
                        self.getProductListData()
                        self.getLocationAndProductData()
                    }
                    else
                    {
                        
                    }
                    
                    print(self.jsonProductClassData)
                }
                
            }
        })
        task.resume()
    }
    func segmentChange(notification: Notification)
    {
        self.viewWillAppear(true)
    }
    
    func stepCheck()
    {
        
        if(simulateValue == "RealTime")
        {
            if(getSteps == "PRODUCT")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
            }
            else
            {
                self.saveButton.isHidden = true
                self.commitButton.isHidden = true
            }
        }
        
    }

}

