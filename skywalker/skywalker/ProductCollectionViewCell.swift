//
//  ProductCollectionViewCell.swift
//  skywalker
//
//  Created by Sabari on 2/13/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productTitle: UILabel!
}
