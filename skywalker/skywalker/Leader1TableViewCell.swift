//
//  Leader1TableViewCell.swift
//  skywalker
//
//  Created by Sabari on 30/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class Leader1TableViewCell: UITableViewCell {

    @IBOutlet var Score: UILabel!
    @IBOutlet var teamName: UILabel!
    @IBOutlet var rankValue: UILabel!
    @IBOutlet var view2: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
