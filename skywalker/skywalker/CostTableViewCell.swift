//
//  CostTableViewCell.swift
//  skywalker
//
//  Created by Sabari on 2/15/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class CostTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var costText: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
