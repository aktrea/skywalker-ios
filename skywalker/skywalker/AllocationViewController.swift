//
//  AllocationViewController.swift
//  skywalker
//
//  Created by Sabari on 2/15/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class AllocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    var urlStrings = ApiUrl()
    var jsonAllocationData = NSArray()
    var playerValueGameVariable = NSArray()
    var getValue = [[Int]]()
    var checkAllConstraints = Bool()
    var checkState = Int()
    var getValueBySection = [[Int]]()
    var gameVariableIdValue = [AnyObject]()
    var checkStatus = Int()
    var urlString = String()//allocation1 url
    var urlString1 = String()//allocation2 url
    var checkStatusCount = Int()
    var checkIntialState = Int()
    var segment = ViewController()
    var allocationPageValue = Int()
    var simulateValue = String()
    var getSteps = String()

    
    
    @IBOutlet weak var allocationTable: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var commitButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var allocation1View: UIView!
    override func viewDidLoad() {
        
        simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String

        super.viewDidLoad()
        
        //set corner radius for save and commit button
        self.commitButton.layer.cornerRadius=5
        self.commitButton.layer.masksToBounds=true
        self.saveButton.layer.cornerRadius=5
        self.saveButton.layer.masksToBounds=true
        self.allocation1View.isHidden = true
        self.nextButton.isHidden = true
        self.saveButton.isHidden = true
        self.commitButton.isHidden = true
        self.checkAllConstraints = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AllocationViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        allocationTable.addGestureRecognizer(tapGesture)
        NotificationCenter.default.addObserver(self, selector: #selector(AllocationViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AllocationViewController.methodOfReceivedNotification1(notification:)), name: Notification.Name("NotificationIdentifierForHideButton"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(AllocationViewController.segmentChange(notification:)), name: Notification.Name("RedirectSegment"), object: nil)
        
        for i in 0..<SegmentViewController.RealTime.realtimeTabArray.count
        {
            if(SegmentViewController.RealTime.realtimeTabArray[i] == "FINANCIAL")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
                getSteps = "FINANCIAL"
                break
            }
            else
            {
                self.saveButton.isUserInteractionEnabled = false
                self.commitButton.isUserInteractionEnabled = false
                getSteps = ""
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkIsCommit()
    }
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //
    //       // return (playerValueGameVariable[0] as AnyObject).count
    //        return 4
    //
    //    }
    //    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //
    //        return jsonAllocationData.count
    //    }
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    //    {
    //
    //        let sectionValue = (jsonAllocationData[section] as AnyObject).value(forKey: "LocationName") as! String
    //        return sectionValue
    //    }
    //    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //
    //        let cell = allocationTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AllocationTableViewCell
    //        cell.backgroundColor = UIColor.clear
    //        let Value = (jsonAllocationData[indexPath.row] as AnyObject).value(forKey: "LocationName") as! String
    //         cell.gameVariableLabel.text = Value
    //        return cell
    //    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return jsonAllocationData.count;
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (playerValueGameVariable[section] as AnyObject).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = allocationTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AllocationTableViewCell
        cell.backgroundColor = UIColor.clear
        let gameVariableName = (playerValueGameVariable[indexPath.section] as AnyObject).value(forKey: "GameVariableName") as! NSArray
        let gameValue = (playerValueGameVariable[indexPath.section] as AnyObject).value(forKey: "Value") as! NSArray
        cell.gameVariableLabel.text = gameVariableName[indexPath.row] as? String
        cell.textValue.text = String(describing: getValue[indexPath.section][indexPath.row])
        cell.textValue.delegate=self
        cell.textValue.tag=(indexPath.section * 1000) + indexPath.row
        cell.textValue.keyboardType = .numberPad
        if(getSteps == "FINANCIAL" || simulateValue != "RealTime")
        {
        cell.textValue.addTarget(self, action: #selector(self.getText), for: .editingDidEnd)
        }
        else
        {
          Util.showNotification(title: "",subTitle: urlStrings.bannerMessage ,color: Util.BannerColors.blue)
           cell.textValue.isUserInteractionEnabled=false
        }
        
        return cell
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        
        let sectionValue = (jsonAllocationData[section] as AnyObject).value(forKey: "LocationName") as! String
        return sectionValue
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        if(tableView == allocationTable)
        {
            view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.textColor = UIColor.white
        }
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    /*!
     *@brief get get GameVariable data (Allocation data) from server and reload the tableview to update the data
     */
    func getAllocationListData()
    {
        let url = urlStrings.getPlayerLocationIdandGameVariableId
        let urlValue = URL(string: "\(url)\(7)")!//stepid =7
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        
                        self.jsonAllocationData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        print(self.jsonAllocationData)
                        self.playerValueGameVariable = self.jsonAllocationData.value(forKey: "playerValueGameVariable") as! NSArray
                        for i in 0..<self.jsonAllocationData.count
                        {
                            let gameValue = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "Value") as! NSArray
                            var valueArray = [Int]()
                            for j in 0..<(self.playerValueGameVariable[i] as AnyObject).count
                            {
                                valueArray.append(gameValue[j] as! Int)
                                //When load at 1st time check fro is selected and check state to enable save,commit nd revoke button
                                let isSelected = (self.self.playerValueGameVariable[i] as AnyObject).value(forKey: "IsSelected") as! NSArray
                                let state = (self.self.playerValueGameVariable[i] as AnyObject).value(forKey: "CheckState") as! NSArray
                                if(isSelected[j]) as! Bool
                                {
                                    //for new user Is selected is 0 this loop is not execute for new user
                                    self.checkIntialState = 1
                                }
                                if(state[i] as! Int != 0)
                                {
                                    self.checkStatusCount = 1;
                                    self.checkStatus = state[i] as! Int
                                }
                                
                            }
                            self.getValue.append(valueArray as [Int])
                            print(self.getValue)
                            
                        }
                        if(self.checkStatusCount == 0)
                        {
                            self.checkStatus = 0
                        }
                        //In First time(new user) check State is 0.
                        //if checkstatus = 2 save button intraction isenable
                        //set checkStatus =2
                        //now savebutton will be enable at new user
                        if(self.checkIntialState == 0)
                        {
                            self.checkStatus = 2
                        }
                        if(self.allocationPageValue == 1)
                        {
                            if(self.checkStatus == 0)
                            {
                                DispatchQueue.main.async()
                                    {
                                        //set border for enabled button
                                        self.commitButton.layer.borderColor = UIColor.white.cgColor
                                        self.commitButton.layer.borderWidth=1
                                        self.saveButton.layer.borderWidth=0
                                        self.saveButton.isHidden=false
                                        self.saveButton.isUserInteractionEnabled = false
                                        self.commitButton.isUserInteractionEnabled=true
                                        self.commitButton.setTitle("Commit",for: .normal)
                                }
                            }
                            else  if(self.checkStatus == 2)
                            {
                                DispatchQueue.main.async()
                                    {
                                        //set border for enabled button
                                        self.saveButton.layer.borderColor = UIColor.white.cgColor
                                        self.saveButton.layer.borderWidth=1
                                        self.commitButton.layer.borderWidth=0
                                        self.saveButton.isHidden=false
                                        self.saveButton.isUserInteractionEnabled = true
                                        self.commitButton.isUserInteractionEnabled=false
                                        self.commitButton.setTitle("Commit",for: .normal)
                                }
                            }
                            else  if(self.checkStatus == 1)
                            {
                                DispatchQueue.main.async()
                                    {
                                        self.commitButton.layer.borderColor = UIColor.white.cgColor
                                        self.commitButton.layer.borderWidth=1
                                        self.saveButton.isHidden=true
                                        self.commitButton.isUserInteractionEnabled=true
                                        self.commitButton.setTitle("Revoke",for: .normal)
                                }
                                
                            }
                        }
                        DispatchQueue.main.async(){
                            self.stepCheck()
                            self.allocationTable.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    
    
    @IBAction func nextButton(_ sender: Any) {
        
        if(self.nextButton.currentTitle == "Next")
        {
            self.allocation1View.isHidden = false
            self.saveButton.isHidden = false
            self.commitButton.isHidden = false
            self.allocationPageValue = 1
            getAllocationListData()
            self.nextButton.setTitle("Pre", for: .normal)
        }
        else if(self.nextButton.currentTitle == "Pre")
        {
            self.allocationPageValue = 0
            self.getAllocationListData()
            self.allocation1View.isHidden = true
            self.saveButton.isHidden = true
            self.commitButton.isHidden = true
            self.nextButton.setTitle("Next", for: .normal)
        }
    }
    
    func hideKeyboard() {
        allocationTable.endEditing(true)
    }
    //get user entered value (constraints)
    func getText(_ sender: Any) {
        
        let textField: UITextField? = (sender as? UITextField)
        let section = (sender as AnyObject).tag/1000
        let row = (sender as AnyObject).tag%1000
        let valueFromText = textField?.text
        print(getValue)
        getValue[section][row] = Int(valueFromText!)!
        var checkValue = Int()
        var checkAllValue = Int()
        var gameVariableValues = NSArray()
        //addition opertion execute when user enter at last section
        
        if(section == self.jsonAllocationData.count-1)
        {
            for i in 0..<self.jsonAllocationData.count
            {
                gameVariableValues = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "GameVariableName") as! NSArray
                for j in 0..<(self.playerValueGameVariable[i] as AnyObject).count
                {
                    if(j==row)
                    {
                        checkValue = checkValue+getValue[i][j] as Int
                    }
                }
            }
            //check value equal to 100
            //if not alert will display for related particular gameVariableName
            if(checkValue != 100)
            {
                let remainValue = 100 - checkValue as! Int
                let alertController = UIAlertController(title: "\(gameVariableValues[row]) for all Location", message: "Total value should be equal to 100", preferredStyle: UIAlertControllerStyle.alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                {
                    (result : UIAlertAction) -> Void in
                    textField?.text = String(checkValue+remainValue)
                    
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
            
        }
        //check the all value equal to 100 or not
        //if equal to 100 next button is visible
        for j in 0..<getValue[0].count
        {
            for k in 0..<getValue.count
            {
                checkAllValue = checkAllValue+getValue[k][j]
            }
            if(checkAllValue != 100)
            {
                checkAllConstraints = false
                checkAllValue = 0 //set value to 0 for another loop execution
            }
            else
            {
                checkAllValue = 0
                checkState = checkState+1
            }
        }
        print(checkState)
        if(checkAllConstraints == true || checkState == getValue[0].count)
        {
            nextButton.isHidden=false
            saveButton.isHidden = true
            commitButton.isHidden = true
        }
        //        else
        //        {
        //            nextButton.isHidden=true
        //            saveButton.isHidden = false
        //            commitButton.isHidden = false
        //        }
        checkState=0 //set value to 0 for another loop execution starts from 0
        
    }
    func methodOfReceivedNotification(notification: Notification){
        saveButton.isHidden = false
        commitButton.isHidden = false
        nextButton.isHidden = true
    }
    func methodOfReceivedNotification1(notification: Notification){
        saveButton.isHidden = true
        commitButton.isHidden = true
        nextButton.isHidden = false
    }
    @IBAction func saveButton(_ sender: Any) {
        urlString1 = urlStrings.savePlayeProductVariable
        urlString = urlStrings.savePlayerGameVariable
        saveAllocation()
        saveAllocation1()
        //send notification when user in realtime simulation
        if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
        {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" FinancialSaved"])
        }
    }
    @IBAction func commitButton(_ sender: Any) {
        if(self.checkStatus == 1)
        {
            urlString1 = urlStrings.revokePlayeProductVariable
            urlString = urlStrings.revokePlayerGameVariable
            saveAllocation()
            saveAllocation1()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Financial Revoked"])
            }
        }
        else  if(self.checkStatus == 0)
        {
            urlString1 = urlStrings.submitPlayeProductVariable
            urlString = urlStrings.submitPlayerGameVariable
            saveAllocation()
            saveAllocation1()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Financial Commited"])
            }
        }
        
    }
    func saveAllocation()
    {
        var getValue = [Int]()
        var getelement = [String : Any]()
        var peopleJsonarr = [Any]()
        
        
        //get values from each section and row
        //i(section) j(row) store value in array base on section
        for i in 0..<jsonAllocationData.count
        {
            for j in 0..<(playerValueGameVariable[i] as AnyObject).count
            {
                let indexPath = IndexPath(row: j, section: i)
                let cell = allocationTable.cellForRow(at: indexPath) as! AllocationTableViewCell
                let textValue = cell.textValue.text!
                getValue.append(Int(textValue)!)
            }
            getValueBySection.append(getValue)
            getValue.removeAll()
        }
        //construct json data from tableview(section and row)
        for i in 0..<jsonAllocationData.count
        {
            for j in 0..<(playerValueGameVariable[i] as AnyObject).count
            {
                let gameVariableid = (playerValueGameVariable[i] as AnyObject).value(forKey: "GameVariableId") as! NSArray
                let gameVariable = ["GameVariableId":gameVariableid[j],"Value":getValueBySection[i][j]] as [String : Any]
                gameVariableIdValue.append(gameVariable as AnyObject)
            }
            //store gamevariablevalue as array by row count
            getelement = ["LocationId":(jsonAllocationData[i] as AnyObject).value(forKey: "locationId")!,"stepId":"7","GameVariableValue":gameVariableIdValue] as [String : Any]
            gameVariableIdValue.removeAll()
            peopleJsonarr.append(getelement)
            
        }
        print(peopleJsonarr)
        //send json data to server
        let jsonData = try? JSONSerialization.data(withJSONObject: peopleJsonarr, options: .prettyPrinted)
        let decodedValue = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSArray
        
        let jsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
        let urlValue = URL(string: urlString)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData1
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(postResponse)
                        if(self.checkStatus == 0)
                        {
                            let sendSegmentValue = ["SegmentValue":self.segment.segmentValue+1]
                            print(sendSegmentValue)
                            NotificationCenter.default.post(name: Notification.Name("RedirectSegment"), object:nil,userInfo:sendSegmentValue)
                        }
                        
                        //after save checkStatus set to 0
                        if(self.checkStatus == 2)
                        {
                            if(postResponse == "true")
                            {
                                self.checkStatus=0
                                self.getValue.removeAll()
                                self.getAllocationListData()
                            }
                        }
                        else
                        {
                            if(postResponse == "true")
                            {
                                self.getValue.removeAll()
                                self.getAllocationListData()
                            }
                        }
                        
                        
                    }
                    
                }
                
        })
        
        task.resume()
        
    }
    
    func saveAllocation1()
    {
        let allocation1Data = UserDefaults.standard.value(forKey: "allocation1_data") as! NSArray
        
        //send json data to server
        let jsonData = try? JSONSerialization.data(withJSONObject: allocation1Data, options: .prettyPrinted)
        let decodedValue = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSArray
        
        let jsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
        let urlValue = URL(string: urlString1)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData1
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(postResponse)
                        
                    }
                    
                }
                
        })
        
        task.resume()
        
    }
    func checkIsCommit()
    {
        let url = urlStrings.isCommit
        let urlValue = URL(string: "\(url)\(7)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                    print(postResponse)
                    if(postResponse == "true")
                    {
                        self.getAllocationListData()
                    }
                    else
                    {
                        
                    }
                    
                    
                }
                
            }
        })
        task.resume()
    }
    func segmentChange(notification: Notification)
    {
        self.viewWillAppear(true)
    }
    /*!
     *@brief While enter username and password keyboardDidShow fuction execute(NSNotification).
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardDidShow, object: nil);
        return true
        
    }
    /*!
     *@brief keyboard disappear when click return/done button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*!
     *@brief after enter username and password keyboardDidHide fuction execute(NSNotification).
     */
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil);
        return true
        
        
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*!
     *@brief move view to top when keyboard appears
     */
    func keyboardDidShow(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.allocationTable.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.allocationTable.frame = tvFrame
    }
    /*!
     *@brief set view to orginal height when keyboard hide
     */
    func keyboardDidHide(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.allocationTable.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.allocationTable.frame = tvFrame
    }
    //check for step read access
    func stepCheck()
    {
        
        if(simulateValue == "RealTime")
        {
            if(getSteps == "FINANCIAL")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
            }
            else
            {
                self.saveButton.isHidden = true
                self.commitButton.isHidden = true
            }
        }
    }

}
