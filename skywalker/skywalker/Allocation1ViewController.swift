//
//  Allocation1ViewController.swift
//  skywalker
//
//  Created by Sabari on 3/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class Allocation1ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate
{
    var jsonAllocationData1 = NSArray()
    var playerValueGameVariable1 = NSArray()
    var tableData = NSArray()
    var urlStrings = ApiUrl()
    var getValue = [[Int]]()
    var getValuebyLocation = [Any]()
    var checkAllConstraints = Bool()
    var checkState = Int()
    var getselectIndexpath = Int()
    var gameVariableIdValue = [AnyObject]()
    var playerProductValue = [AnyObject]()
    
    @IBOutlet weak var allocationCollection: UICollectionView!
    
    @IBOutlet weak var allocation1Table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AllocationViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        allocation1Table.addGestureRecognizer(tapGesture)
        self.getPlayerProductandGameVariable()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*!
     *@brief get PlayerProductandGameVariable (Allocation data) from server and reload the tableview to update the data
     */
    func getPlayerProductandGameVariable()
    {
        let url = urlStrings.getPlayerProductandGameVariable
        let urlValue = URL(string: "\(url)\(7)")!//stepid =7
        
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonAllocationData1 = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        for x in 0..<self.jsonAllocationData1.count
                        {
                            self.playerValueGameVariable1 =  (self.jsonAllocationData1[x] as AnyObject).value(forKey: "playerLocationProduct") as! NSArray
                            self.tableData = self.playerValueGameVariable1
                            print(self.playerValueGameVariable1)
                            for i in 0..<self.tableData.count
                            {
                                let playerValue = (self.tableData[i] as AnyObject).value(forKey: "playerValueGameVariable") as! NSArray
                                let gameValue = playerValue.value(forKey: "Value") as! NSArray
                                var valueArray = [Int]()
                                for j in 0..<playerValue.count
                                {
                                    valueArray.append(gameValue[j] as! Int)
                                }
                                self.getValue.append(valueArray as [Int])
                            }
                            print(self.getValue)
                            self.getValuebyLocation.append(self.getValue as [[Int]])
                            self.getValue.removeAll()
                        }
                        
                        DispatchQueue.main.async(){
                            self.allocationCollection.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.jsonAllocationData1.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let label = cell.contentView.viewWithTag(1) as! UILabel
        let labelValue = self.jsonAllocationData1.value(forKey: "LocationName") as! NSArray
        label.text = labelValue[indexPath.row] as? String
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.playerValueGameVariable1 =  (self.jsonAllocationData1[indexPath.item] as AnyObject).value(forKey: "playerLocationProduct") as! NSArray
        tableData = self.playerValueGameVariable1
        getselectIndexpath = indexPath.row
        //        print(self.playerValueGameVariable1)
        //        for i in 0..<self.tableData.count
        //        {
        //            let playerValue = (self.tableData[i] as AnyObject).value(forKey: "playerValueGameVariable") as! NSArray
        //            let gameValue = playerValue.value(forKey: "Value") as! NSArray
        //            var valueArray = [Int]()
        //            for j in 0..<playerValue.count
        //            {
        //                valueArray.append(gameValue[j] as! Int)
        //            }
        //            self.getValue.append(valueArray as [Int])
        //        }
        DispatchQueue.main.async(){
            self.allocation1Table.reloadData()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(playerValueGameVariable1)
        if self.tableData.count==0
        {
            return 0
        }
        else
        {
            let playerValue = (self.tableData[section] as AnyObject).value(forKey: "playerValueGameVariable") as! NSArray
            return playerValue.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.allocation1Table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Allocation1TableViewCell
         cell.backgroundColor = UIColor.clear
        let playerValue = (self.tableData[indexPath.section] as AnyObject).value(forKey: "playerValueGameVariable") as! NSArray
        let gameVariable = playerValue.value(forKey: "GameVariableName") as! NSArray
        let gameValue = playerValue.value(forKey: "Value") as! NSArray
        cell.gameVariableLabel.text = gameVariable[indexPath.row] as? String
        cell.textFieldValue.text = String(describing: gameValue[indexPath.row])
        cell.textFieldValue.delegate=self
        cell.textFieldValue.tag=(indexPath.section * 1000) + indexPath.row
        cell.textFieldValue.keyboardType = .numberPad
        cell.textFieldValue.addTarget(self, action: #selector(self.getText), for: .editingDidEnd)
        return cell
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        if self.tableData.count==0
        {
            return ""
        }
        else
        {
            let sectionValue = self.tableData.value(forKey: "ProductName") as! NSArray
            print(sectionValue)
            return sectionValue[section] as? String
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func hideKeyboard()
    {
        allocation1Table.endEditing(true)
    }
    //get user entered value (constraints)
    func getText(_ sender: Any)
    {
        
        let textField: UITextField? = (sender as? UITextField)
        let section = (sender as AnyObject).tag/1000
        let row = (sender as AnyObject).tag%1000
        let valueFromText = textField?.text
        getValue = getValuebyLocation[getselectIndexpath] as! [[Int]]
        getValue[section][row] = Int(valueFromText!)!
        getValuebyLocation[getselectIndexpath] = getValue
        print(getValuebyLocation)
        
        var checkValue = Int()
        var checkAllValue = Int()
        //check the all value equal to 100 or not
        //if equal to 100
        for j in 0..<getValue.count
        {
            for k in 0..<getValue[j].count
            {
                checkAllValue = getValue[j][k]
                print(checkAllValue)
                if(checkAllValue != 100)
                {
                    checkAllConstraints = false
                    checkAllValue = 0 //set value to 0 for another loop execution
                }
                else
                {
                    checkAllValue = 0
                    checkState = checkState+1
                }
            }
        }
        print(checkState)
        if(checkAllConstraints == true || checkState == getValue.count * getValue[0].count )
        {
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
            saveAllocation1()
        }
        else
        {
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierForHideButton"), object: nil)
            print("save and commit not enable")
        }
        checkState=0 //set value to 0 for another loop execution starts from 0
    }
    func saveAllocation1()
    {
        var getelement = [String : Any]()
        var getelement1 = [String : Any]()
        
        var peopleJsonarr = [Any]()
        for x in 0..<self.jsonAllocationData1.count
        {
            let locationId = (self.jsonAllocationData1[x] as AnyObject).value(forKey: "locationId") as! Int
            getValue = getValuebyLocation[x] as! [[Int]]
            self.playerValueGameVariable1 =  (self.jsonAllocationData1[x] as AnyObject).value(forKey: "playerLocationProduct") as! NSArray
            self.tableData = self.playerValueGameVariable1
            for i in 0..<tableData.count
            {
                let playerProductId = (self.tableData[i] as AnyObject).value(forKey: "PlayerProductId") as! Int
                let productId = (self.tableData[i] as AnyObject).value(forKey: "productId") as! Int
                let playerValue = (self.tableData[i] as AnyObject).value(forKey: "playerValueGameVariable") as! NSArray
                
                
                for j in 0..<playerValue.count
                {
                    let gameVariableid = (playerValue[j] as AnyObject).value(forKey: "GameVariableId") as! Int
                    let gameVariable = ["GameVariableId":gameVariableid,"Value":getValue[i][j]] as [String : Any]
                    gameVariableIdValue.append(gameVariable as AnyObject)
                }
                //store gamevariablevalue as array by row count
                getelement = ["PlayerProductId":playerProductId,"ProductId":productId,"stepId":"7","GameVariableValue":gameVariableIdValue] as [String : Any]
                gameVariableIdValue.removeAll()
                playerProductValue.append(getelement as AnyObject)
            }
            getelement1 = ["LocationId":locationId,"playerProduct":playerProductValue] as [String : Any]
            playerProductValue.removeAll()
            peopleJsonarr.append(getelement1)
            
        }
        print(peopleJsonarr)
        //save userid in UserDefaults
        let userId = UserDefaults.standard
        userId.set(peopleJsonarr, forKey: "allocation1_data")
        userId.synchronize()
        
    }
    /*!
     *@brief While enter username and password keyboardDidShow fuction execute(NSNotification).
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardDidShow, object: nil);
        return true
        
    }
    /*!
     *@brief keyboard disappear when click return/done button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*!
     *@brief after enter username and password keyboardDidHide fuction execute(NSNotification).
     */
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil);
        return true
        
        
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*!
     *@brief move view to top when keyboard appears
     */
    func keyboardDidShow(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.allocation1Table.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.allocation1Table.frame = tvFrame
    }
    /*!
     *@brief set view to orginal height when keyboard hide
     */
    func keyboardDidHide(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.allocation1Table.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.allocation1Table.frame = tvFrame
    }

}
