//
//  SegmentViewController.swift
//  skywalker
//
//  Created by Sabari on 2/13/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit
class SegmentViewController: MXSegmentedPagerController,MXPagerViewDelegate {
    
    struct RealTime
    {
      static var realtimeTabArray = [String]()
    }
    @IBOutlet var stageName: UILabel!
    @IBOutlet var timerLabel: UILabel!
    @IBOutlet weak var menuBar: UIBarButtonItem!
    @IBOutlet var headerView: UIView!
    var getSelect = Bool()
    var simulateValue = String()
    var getActiveStage = NSDictionary()
    var controllerArray = [UIViewController]()
    var segueArray : [String] = ["mx_page_0","mx_page_1","mx_page_2","mx_page_3","mx_page_4","mx_page_5","mx_page_6","mx_page_7"]
    var tabArray : [String] = ["LOCATION", "PRODUCT", "PROMOTION", "CHANNEL","PEOPLE","COST","FINANCIAL","PRICING"]
   public var count = Int()
    var realTimeSegue = [String]()
    var realtimeTabArray = [String]()
     var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(SegmentViewController.update), userInfo: nil, repeats: true)
        self.segmentedPager.delegate = self
        controllerArray = [ViewController(),ProductViewController(),PromotionViewController(),ChannelViewController(),PeopleViewController(),CostViewController(),FinancialViewController(),PricingViewController()]
        
        //get notification from signalr increase and decrease by minutes
        NotificationCenter.default.addObserver(self, selector: #selector(SegmentViewController.timer(notification:)), name: Notification.Name("Timer"), object: nil)
        setMenuBarBtn(menuBar)
        //get active Stage
        let outData = UserDefaults.standard.data(forKey: "activeStage_data")
        getActiveStage = NSKeyedUnarchiver.unarchiveObject(with: outData!)! as! NSDictionary
       
        simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String
        if(simulateValue == "Simulation")
        {
            getSelect = true
            self.segmentedPager.reloadData()
        }
        else if(simulateValue == "RealTime")
        {
            let stepId = getActiveStage.value(forKey: "stepRoles") as! NSArray
            for i in 0..<stepId.count
            {
                let value =  (stepId[i] as AnyObject).value(forKey: "StepId") as! Int
                self.realTimeSegue.append(segueArray[value-1])
                self.realtimeTabArray.append(tabArray[value-1])
            }
            getSelect = false
            print(realtimeTabArray)
            print(realTimeSegue)
            self.segmentedPager.reloadData()
        }
        
        
        
        
        // Parallax Header
        self.segmentedPager.parallaxHeader.view = headerView
        self.segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.fill
        self.segmentedPager.parallaxHeader.height = 64
        self.segmentedPager.parallaxHeader.minimumHeight = 20
        
        //        // Segmented Control customization
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.clear
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        self.segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName : UIColor.orange]
        self.segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.fullWidthStripe
        self.segmentedPager.segmentedControl.selectionIndicatorColor = UIColor.orange
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // menuBar.transitioningDelegate = self
        
    }
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        if(self.getSelect == true)
        {
            print(tabArray.count)
            return tabArray.count
        }
        else
        {
            print(realTimeSegue.count)
            return realTimeSegue.count
            
        }
    }
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        
        if(self.getSelect == true)
        {
            return tabArray[index]
        }
        else
        {
            print(realtimeTabArray[index])
            return realtimeTabArray[index]
        }
        
        
        
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, segueIdentifierForPageAt index: Int) -> String {
        if(self.getSelect == true)
        {
            return segueArray[index]
        }
        else
        {
            return realTimeSegue[index]
        }
    }
    func segmentChange(notification: Notification)
    {
        let indexValue = notification.userInfo!["SegmentValue"] as! Int
        print(indexValue)
        DispatchQueue.main.async()
            {
                if(self.segueArray.count >= indexValue+1)
                {
               // self.segmentedPager.segmentedControl.setSelectedSegmentIndex(UInt(indexValue), animated: true)
                //self.segmentedPager.pager.showPage(at: indexValue, animated: true)
                }
        }
    }
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWithTitle title: String) {
       print(title)
    }
    func timer1()
    {
        timer = Timer(timeInterval: 1.0, target: self, selector: #selector(SegmentViewController.update), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
    }
    //convert totalmilisecohds to hours:minutes:seconds to update timer
    func update() {
        
        if(count > 0){
           count = count-1
            let seconds = count % 60;
            let minutes = (count / 60) % 60;
            let hours = count / 3600;
            self.timerLabel.text = String(hours) + ":" + String(minutes) + ":" + String(seconds)
            print(count)
        }
    }
    //get notification from signalr (increase and decrease by minutes)
    func timer(notification: Notification){
        timer.invalidate()
        
        self.count = notification.userInfo!["TotalMilliSeconds"] as! Int
       self.count = self.count/1000
        self.timer1()
       
    }

}


