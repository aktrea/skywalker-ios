//
//  SimulationViewController.swift
//  skywalker
//
//  Created by Sabari on 15/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//
/*Copyright (c) 2015 Adam Hartford
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.*/

import UIKit

class SimulationViewController: UIViewController {
    //create global variable to access in all ViewController
    struct SimulateCheckStatus{
        static var simulateCheck = String()
    }
    
    
    @IBOutlet weak var menuBar: UIBarButtonItem!
    @IBOutlet var mainView: UIView!
    @IBOutlet var view2: UIView!
    @IBOutlet var view1: UIView!
    var urlStrings = ApiUrl()
    var stageId = Int()
    var stageType = Int()
    var jsonDashBoardData = NSDictionary()
    var gameStages = NSArray()
    var groupId = Int()
    var getStageName = [Any]()
    
    
    
    //signalr
    var chat: Hub!
    var chat1: Hub!
    var hubConnection: SignalR!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dashBoardData()
        NotificationCenter.default.addObserver(self, selector: #selector(SimulationViewController.realTimeCall), name: NSNotification.Name(rawValue: "RealTimeSwitch"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SimulationViewController.simulationCall), name: NSNotification.Name(rawValue: "SimulationSwitch"), object: nil)
        //receive notification when user save,commit,revoke in realtime simulation
        NotificationCenter.default.addObserver(self, selector: #selector(SimulationViewController.saveCommitRevokeStatus), name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil)
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone)
        {
            setMenuBarBtn(menuBar)
        }
        
        
        hubConnection = SignalR(urlStrings.baseUrl)
        hubConnection.useWKWebView = false
        hubConnection.signalRVersion = .v2_2_0
        chat = Hub("EnthiranHub")
        // chat1 = Hub("chatHub")
        hubConnection.addHub(chat)
        hubConnection.start()
        chat.on("IncreaseByMinutes")
        { args in
            let message = args![0] as! NSDictionary
            
            NotificationCenter.default.post(name: Notification.Name("Timer"), object:nil,userInfo:message as? [AnyHashable : Any])
            print("Message: \(message)")
            Util.showNotification(title: "",subTitle: "Time Increased",color: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000))
        }
        //memberSignIn
        chat.on("memberSignIn")
        { args in
            let message = args![0] as! String
            Util.showNotification(title: "",subTitle: message ,color: UIColor(red:255.0/255.0, green:204.0/255.0, blue:51.0/255.0, alpha:1.000))
        }
        chat.on("DecreaseByMinutes")
        { args in
            let message = args![0] as! NSDictionary
            NotificationCenter.default.post(name: Notification.Name("Timer"), object:nil,userInfo:message as? [AnyHashable : Any])
            print("Message: \(message)")
            Util.showNotification(title: "",subTitle: "Time Decreased",color: UIColor(red:255.0/255.0, green:204.0/255.0, blue:51.0/255.0, alpha:1.000))
            
        }
        chat.on("notifyStatus")
        { args in
            let groupName = args![0] as! String
            //            let userName = args![1] as! String
            //            let message = args![1] as! String
            
            Util.showNotification(title: groupName ,subTitle: "",color: UIColor(red:255.0/255.0, green:204.0/255.0, blue:51.0/255.0, alpha:1.000))
            
        }
        chat.on("UpdateGameDetailsList")
        { args in
            let message = args![0] as! NSArray
            
            
            for i in 0..<message.count
            {
                let groupId = (message[i] as AnyObject).value(forKey: "GroupId") as! Int
                
                let StageStatus = (message[i] as AnyObject).value(forKey: "StageStatus") as! Int
                
                if(groupId == self.groupId)
                {
                    if(StageStatus == 1)
                    {
                        Util.showNotification(title: "Stage Activated",subTitle: "",color: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000))
                        self.stageId = (message[i] as AnyObject).value(forKey: "StageId") as! Int
                        self.stageType = (message[i] as AnyObject).value(forKey: "StageType") as! Int
                        self.activeStage()
                        self.realTime()
                    }
                    else if (StageStatus == 0)
                    {
                        Util.showNotification(title: "Stage Stoped",subTitle: "",color: UIColor(red:48.00/255.0, green:174.0/255.0, blue:51.5/255.0, alpha:1.000))
                        
                    }
                    
                }
            }
            
            
            
        }
        
        // hubConnection.addHub(chat1)
        // SignalR events
        
        hubConnection.starting = { [weak self] in
            print("Starting...")
        }
        
        hubConnection.reconnecting = { [weak self] in
            print("Reconnecting...")
            
        }
        
        hubConnection.connected = { [weak self] in
            print("Connected. Connection ID: \(self?.hubConnection.connectionID!)")
            self?.chat.invoke("groupMemberSignIn", arguments: [self?.jsonDashBoardData.value(forKey: "TeamName")!, self?.jsonDashBoardData.value(forKey: "PlayerName")!])
        }
        
        hubConnection.reconnected = { [weak self] in
            print("Reconnected. Connection ID: \(self?.hubConnection.connectionID!)")
            
        }
        
        hubConnection.disconnected = { [weak self] in
            print("Disconnected.")
        }
        
        hubConnection.connectionSlow = { print("Connection slow...") }
        
        hubConnection.error = { [weak self] error in
            print("Error: \(error)")
            
            // Here's an example of how to automatically reconnect after a timeout.
            //
            // For example, on the device, if the app is in the background long enough
            // for the SignalR connection to time out, you'll get disconnected/error
            // notifications when the app becomes active again.
            
            if let source = error?["source"] as? String , source == "TimeoutException" {
                print("Connection timed out. Restarting...")
                self?.hubConnection.start()
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // menuBar.transitioningDelegate = self
        
    }
    //simulation_segue
    @IBAction func realTime(_ sender: Any) {
        //        NotificationCenter.default.post(name: Notification.Name("RealTimeNotify"), object:nil)
        realTime()
    }
    func realTime()
    {
        SimulateCheckStatus.simulateCheck = "realTime"
        let url = urlStrings.isSimulate
        let urlValue = URL(string: "\(url)\(false)")!
        print(urlValue)
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    let userId = UserDefaults.standard
                    userId.set("RealTime", forKey: "simulationSelect")
                    userId.synchronize()
                    
                    DispatchQueue.main.async(){
                        self.performSegue(withIdentifier: "simulation_segue", sender:nil)
                    }
                }
                else
                {
                    let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print(dataString!)
                    let userId = UserDefaults.standard
                    userId.set("RealTime", forKey: "simulationSelect")
                    userId.synchronize()
                    if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone)
                    {
                        DispatchQueue.main.async(){
                            self.performSegue(withIdentifier: "simulation_segue", sender:nil)
                        }
                        
                    }
                    else
                    {
                        //reload iPad screen when user select realtime
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadiPadView"), object: nil)
                    }

                    
                }
        })
        task.resume()
        
    }
    @IBAction func simulation(_ sender: Any) {
        simulation()
        
    }
    func simulation()
    {
        SimulateCheckStatus.simulateCheck = "Simulate"
        let url = urlStrings.isSimulate
        let urlValue = URL(string: "\(url)\(true)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!.localizedDescription)
                }
                else
                {
                    do
                    {
                        let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        if((dataString) != nil)
                        {
                            let userId = UserDefaults.standard
                            userId.set("Simulation", forKey: "simulationSelect")
                            userId.synchronize()
                            DispatchQueue.main.async(){
                                if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone)
                                {
                                    
                                    self.performSegue(withIdentifier: "simulation_segue", sender:nil)
                                }
                                else
                                {
                                    ////reload iPad screen when user select simulation
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadiPadView"), object: nil)
                                }
                            }
                        }
                    }
                    
                }
        })
        task.resume()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "simulation_segue")
        {
            DispatchQueue.main.async(){
                _ = segue.destination
            }
        }
    }
    //get DashboardDetails
    func dashBoardData()
    {
        let urlValue = URL(string: urlStrings.dashBoardData )!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    
                    self.jsonDashBoardData =  try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    //                    self.chat.invoke("groupMemberSignIn", arguments: [self.jsonDashBoardData.value(forKey: "TeamName")!, self.jsonDashBoardData.value(forKey: "PlayerName")!])
                    print(self.jsonDashBoardData)
                    let teamMember = self.jsonDashBoardData.value(forKey: "TeamMembers") as! NSDictionary
                    self.groupId = teamMember.value(forKey: "GroupId") as! Int
                    //save dashBoard in UserDefaults
                    let data = NSKeyedArchiver.archivedData(withRootObject: self.jsonDashBoardData)
                    UserDefaults.standard.set(data, forKey: "dashboard_data")
                    if(( self.jsonDashBoardData.value(forKey: "EnthiranGameStages")) != nil)
                    {
                        self.gameStages = self.jsonDashBoardData.value(forKey: "EnthiranGameStages") as! NSArray
                        //                        for i in 0..<self.gameStages.count
                        //                        {
                        //                            let stageId = (self.gameStages[i] as AnyObject).value(forKey: "StageId") as! Int
                        //                            let stageName = (self.gameStages[i] as AnyObject).value(forKey: "StageName") as! String
                        //
                        //                            let stageData = ["StageName": stageName ,"StageId": stageId] as [String : Any]
                        //                           self.getStageName.append(stageData)
                        //                        }
                        
                        
                        //check for stage status
                        //if stage status ==1 get stageid and stagetype
                        for i in 0..<self.gameStages.count
                        {
                            let stageStatus = (self.gameStages[i] as AnyObject).value(forKey: "StageStatus") as! Int
                            if(stageStatus == 1)
                            {
                                self.stageId = (self.gameStages[i] as AnyObject).value(forKey: "StageId") as! Int
                                self.stageType = (self.gameStages[i] as AnyObject).value(forKey: "StageType") as! Int
                                if(self.stageId != 0)
                                {
                                    self.activeStage()
                                    NotificationCenter.default.post(name: Notification.Name("Stage"), object:nil,userInfo:self.gameStages[i] as? [AnyHashable : Any])
                                }
                                
                                return
                            }
                        }
                        //                    if(self.stageId != 0)
                        //                    {
                        //                        self.activeStage()
                        //                    }
                    }
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        task.resume()
    }
    //pass stageid,stage type to enable session
    func activeStage()
    {
        let url = urlStrings.sessionEnableStage
        let urlValue = URL(string: "\(url)\(stageId)&stageType=\(stageType)&isSimulated=false")!
        print(urlValue)
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let activeStage = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    print(activeStage)
                    //save active stage data
                    let data = NSKeyedArchiver.archivedData(withRootObject: activeStage)
                    UserDefaults.standard.set(data, forKey: "activeStage_data")
                    if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad)
                    {
                        self.simulation()
                    }
                    
                    
                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        task.resume()
        
    }
    @IBAction func simulationSelection(_ sender: Any) {
        switch (sender as AnyObject).selectedSegmentIndex
        {
        case 0:
            
            simulation()
            break
            
        case 1:
            realTime()
            break
            
        default:
            break
            
        }
    }
    func realTimeCall(notification : Notification)
    {
        print("Real Time")
        realTime()
    }
    func simulationCall(notification : Notification)
    {
        print("Simulation")
        simulation()
    }
    //saveCommitRevokeStatus execute when user commit,revoke,save
    //invoke with playername,team name, and message
    func saveCommitRevokeStatus(notification : Notification)
    {
        let message =  notification.userInfo!["Save_Commit_RevokeData"] as! String
        self.chat.invoke("groupStatusNotifications", arguments: [self.jsonDashBoardData.value(forKey: "TeamName")!, self.jsonDashBoardData.value(forKey: "PlayerName")!,message])
    }
}

