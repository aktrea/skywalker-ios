//
//  PricingViewController.swift
//  skywalker
//
//  Created by Sabari on 2/15/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class PricingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var priceTable: UITableView!
    var jsonPricingData = NSArray()
    var playerValueProduct = NSArray()
    var getValueBySection = [[Int]]()
    var playerValuedict = [AnyObject]()
    var checkStatus = Int()
    var urlString = String()
    var checkStatusCount = Int()
    var checkIntialState = Int()
    var urlStrings = ApiUrl()
    var simulatecheck = SimulationViewController()
    var simulateValue = String()
    var getSteps = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String
        checkStatusCount = 0
        checkIntialState = 0
        self.commitButton.layer.cornerRadius=5
        self.commitButton.layer.masksToBounds=true
        self.saveButton.layer.cornerRadius=5
        self.saveButton.layer.masksToBounds=true
        //recognize when user touch outside
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AllocationViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        priceTable.addGestureRecognizer(tapGesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(PricingViewController.segmentChange(notification:)), name: Notification.Name("RedirectSegment"), object: nil)
        
        
        for i in 0..<SegmentViewController.RealTime.realtimeTabArray.count
        {
            if(SegmentViewController.RealTime.realtimeTabArray[i] == "PRICING")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
                getSteps = "PRICING"
                break
            }
            else
            {
                self.saveButton.isUserInteractionEnabled = false
                self.commitButton.isUserInteractionEnabled = false
                getSteps = ""
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkIsCommit()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.jsonPricingData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (playerValueProduct[section] as AnyObject).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = priceTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        let playerProductName = (playerValueProduct[indexPath.section] as AnyObject).value(forKey: "ProductName") as! NSArray
        let playerProductName1 = (playerValueProduct[indexPath.section] as AnyObject).value(forKey: "PlayerProductName") as! NSArray
        let playerProductValue = (playerValueProduct[indexPath.section] as AnyObject).value(forKey: "Value") as! NSArray
        let label = cell.contentView.viewWithTag(1) as! UILabel
        let textField1 = cell.contentView.viewWithTag(2) as! UITextField
        let textField2 = cell.contentView.viewWithTag(3) as! UITextField
        if(playerProductName[indexPath.row] is NSNull)
        {
          label.text = playerProductName1[indexPath.row] as? String
        }
        else
        {
        label.text = playerProductName[indexPath.row] as? String
        }
        if(getSteps == "PRICING" || simulateValue != "RealTime")
        {
            textField1.isUserInteractionEnabled=true
            textField2.isUserInteractionEnabled=true
        }
        else
        {
            Util.showNotification(title: "",subTitle: urlStrings.bannerMessage ,color: Util.BannerColors.blue)
            textField1.isUserInteractionEnabled=false
            textField2.isUserInteractionEnabled=false
        }
        textField2.text="\(playerProductValue[indexPath.row])"

        return cell
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let sectionValue = (self.jsonPricingData[section] as AnyObject).value(forKey: "LocationName") as! String
        return sectionValue
    }
    //header color
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    /*!
     *@brief get pricing data  from server and reload the tableview to update the data(country,product name)
     */
    func getPricingData()
    {
        let url = urlStrings.getPlayerLocationandProductId
        let urlValue = URL(string: "\(url)\(8)")!//stepid =8
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonPricingData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        self.playerValueProduct = self.jsonPricingData.value(forKey: "playerValueProduct") as! NSArray
                        for i in 0..<self.jsonPricingData.count
                        {
                            
                            for j in 0..<(self.self.playerValueProduct[i] as AnyObject).count
                            {
                                let isSelected = (self.self.playerValueProduct[i] as AnyObject).value(forKey: "IsSelected") as! NSArray
                                let state = (self.self.playerValueProduct[i] as AnyObject).value(forKey: "CheckState") as! NSArray
                                if(isSelected[j]) as! Bool
                                {
                                    //for new user Is selected is 0 this loop is not execute for new user
                                    self.checkIntialState = 1
                                }
                                if(state[j] as! Int != 0)
                                {
                                    self.checkStatusCount = 1;
                                    self.checkStatus = state[j] as! Int
                                }
                            }
                        }
                        print(self.checkStatus)
                        if(self.checkStatusCount == 0)
                        {
                            self.checkStatus = 0
                        }
                        //In First time(new user) check State is 0.
                        //if checkstatus = 2 save button intraction isenable
                        //set checkStatus =2
                        //now savebutton will be enable at new user
                        if(self.checkIntialState == 0)
                        {
                            self.checkStatus = 2
                        }
                        if(self.checkStatus == 0)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = false
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 2)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.saveButton.layer.borderColor = UIColor.white.cgColor
                                    self.saveButton.layer.borderWidth=1
                                    self.commitButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = true
                                    self.commitButton.isUserInteractionEnabled=false
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 1)
                        {
                            DispatchQueue.main.async()
                                {
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.isHidden=true
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Revoke",for: .normal)
                            }
                            
                        }

                        DispatchQueue.main.async(){
                            //check for step read access
                            self.stepCheck()
                            self.priceTable.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }

    
    @IBOutlet weak var commitButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBAction func commitButton(_ sender: Any) {
        if(self.checkStatus == 1)
        {
            urlString = urlStrings.revokePlayerProduct
            savePricing()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Pricing Revoked"])
            }
            
        }
        else  if(self.checkStatus == 0)
        {
            urlString = urlStrings.submitPlayerProduct
            savePricing()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Pricing Commited"])
            }
        }

    }
    @IBAction func save(_ sender: Any) {
        urlString = urlStrings.savePlayerProduct
        savePricing()
        //send notification when user in realtime simulation
        if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
        {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Pricing Saved"])
        }
        
    }
    func savePricing()
    {
        var getValue = [Int]()
        var getelement = [String : Any]()
        var pricingJsonarr = [Any]()
        //get values from each section and row
        //i(section) j(row) store value in array base on section
        for i in 0..<self.jsonPricingData.count
        {
            for j in 0..<(self.playerValueProduct[i] as AnyObject).count
            {
                let indexPath = IndexPath(row: j, section: i)
                let cell: UITableViewCell? = priceTable.cellForRow(at: indexPath)!
                let textField = cell!.contentView.viewWithTag(3) as! UITextField
                let textValue = textField.text!
                getValue.append(Int(textValue)!)
                
            }
            getValueBySection.append(getValue)
            getValue.removeAll()
        }
        
        print(getValueBySection)
        
        //construct json data from tableview(section and row)
        for i in 0..<self.jsonPricingData.count
        {
            for j in 0..<(self.playerValueProduct[i] as AnyObject).count
            {
                let productIds = (self.playerValueProduct[i] as AnyObject).value(forKey: "ProductId") as! NSArray
                let gameVariable = ["ProductId":productIds[j] as! Int,"Value":String(getValueBySection[i][j]), "playerProductId":0] as [String : Any]
                playerValuedict.append(gameVariable as AnyObject)
            }
            getelement = ["SaveLocationId":(self.jsonPricingData[i] as AnyObject).value(forKey: "locationId") as! Int,"stepId":8,"playervaluePricingProduct":playerValuedict] as [String : Any]
            playerValuedict.removeAll()
            pricingJsonarr.append(getelement)
        }
        print(pricingJsonarr)
        //send json data to server
        let jsonData = try? JSONSerialization.data(withJSONObject: pricingJsonarr, options: .prettyPrinted)
        let decodedValue = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSArray
        print(decodedValue!)
        let jsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
        let urlValue = URL(string: urlString)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData1
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(postResponse)
                        //after save checkStatus set to 0
                        if(self.checkStatus == 2)
                        {
                            if(postResponse == "true")
                            {
                                self.checkStatus=0
                                 self.getPricingData()
                            }
                        }
                        else
                        {
                            if(postResponse == "true")
                            {
                                 self.getPricingData()
                            }
                        }
                        

                       
                    }
                    
                }
                
        })
        
        task.resume()

    }
    func hideKeyboard() {
        priceTable.endEditing(true)
    }
    func checkIsCommit()
    {
        let url = urlStrings.isCommit
        let urlValue = URL(string: "\(url)\(2)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                    print(postResponse)
                    if(postResponse == "true")
                    {
                       self.getPricingData()
                    }
                    else
                    {
                        
                    }
                  
                }
                
            }
        })
        task.resume()
    }

    func segmentChange(notification: Notification)
    {
        self.viewWillAppear(true)
    }
    /*!
     *@brief While enter username and password keyboardDidShow fuction execute(NSNotification).
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardDidShow, object: nil);
        return true
        
    }
    /*!
     *@brief keyboard disappear when click return/done button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*!
     *@brief after enter username and password keyboardDidHide fuction execute(NSNotification).
     */
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil);
        return true
        
        
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*!
     *@brief move view to top when keyboard appears
     */
    func keyboardDidShow(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.priceTable.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.priceTable.frame = tvFrame
    }
    /*!
     *@brief set view to orginal height when keyboard hide
     */
    func keyboardDidHide(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.priceTable.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.priceTable.frame = tvFrame
    }
    //check for step read access
    func stepCheck()
    {
        
        if(simulateValue == "RealTime")
        {
            if(getSteps == "PRICING")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
            }
            else
            {
                self.saveButton.isHidden = true
                self.commitButton.isHidden = true
            }
        }
    }

}
