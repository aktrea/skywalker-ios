//
//  SideMenuController.swift
//  SideBarMenu-Swift
//
//  Created by Wim on 8/3/16.
//  Copyright © 2016 Kwikku. All rights reserved.
//

import Foundation
import UIKit

class SideMenuController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var profilePicture: UIImageView!
    var urlStrings = ApiUrl()
    var jsonDashBoardData = NSDictionary()
    var teamMember = NSDictionary()

    @IBOutlet var simulationSwitch: UISwitch!
    @IBOutlet var simulationLabel: UILabel!
    @IBAction func TeamButton(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "teamMember") as! DashboardViewController
        nextViewController.dashBoardData = jsonDashBoardData
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        view.window!.layer.add(transition, forKey: kCATransition)
        self.present(nextViewController, animated:false, completion:nil)
    }
    @IBOutlet var groupName: UILabel!
    @IBOutlet var teamName: UILabel!
    @IBOutlet var playerName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    let arrayTitle = ["Decisions","Score","Report","Key Analytics","Logout"]
    let arrayIcon = ["ic_home_36pt","ic_person_36pt","ic_person_36pt","ic_star_36pt","ic_settings_36pt"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        NotificationCenter.default.addObserver(self, selector: #selector(SideMenuController.realTimeNotify(notification:)), name: Notification.Name("RealTimeNotify"), object: nil)
        // Retrieving dashboard data as dictionary
        let outData = UserDefaults.standard.data(forKey: "dashboard_data")
        jsonDashBoardData = NSKeyedUnarchiver.unarchiveObject(with: outData!)! as! NSDictionary
        self.playerName.text = self.jsonDashBoardData.value(forKey: "PlayerName") as! String?
        self.teamName.text = self.jsonDashBoardData.value(forKey: "GameIntoduction") as! String?
        self.teamMember = self.jsonDashBoardData.value(forKey: "TeamMembers") as! NSDictionary
        let value = self.teamMember.value(forKey: "GroupName") as! String
        self.groupName.text = value

        
        profilePicture.layer.borderColor = UIColor.white.cgColor
        
        profilePicture.layer.borderWidth = 1.5
        
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SideBarCell = tableView.dequeueReusableCell(withIdentifier: "SideBarCell") as! SideBarCell
        
        cell.tittle.text = arrayTitle[indexPath.row]
        cell.icon.image = UIImage(named: arrayIcon[indexPath.row])
        cell.icon.tintColor=UIColor(red:33/255, green:150/255, blue:243/255, alpha:1.0)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (indexPath.row) {
        case 0:
            self.performSegue(withIdentifier: "home_segue", sender: self)
            break
            
        case 1:
            self.performSegue(withIdentifier: "score", sender: self)
            break

        case 2:
            self.performSegue(withIdentifier: "report", sender: self)
            break
        case 3:
            self.performSegue(withIdentifier: "keymetrics", sender: self)
            break
        case 4:
            logout()
            break
        case 5:
            self.performSegue(withIdentifier: "settings_segue", sender: self)
            break
        default:
            break
        }
    }
    
    //get productlist from server
    func logout()
    {
        let urlValue = URL(string: urlStrings.getProducts )!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let dataLogutString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    
                    print(dataLogutString!)
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    self.present(nextViewController, animated:true, completion:nil)
                }
                
            }
        })
        task.resume()
    }
    //get DashboardDetails
    func dashBoardData()
    {
        let urlValue = URL(string: urlStrings.dashBoardData )!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    self.jsonDashBoardData =  try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    print(self.jsonDashBoardData)
                    DispatchQueue.main.async(){
                    self.playerName.text = self.jsonDashBoardData.value(forKey: "PlayerName") as! String?
                    self.teamName.text = self.jsonDashBoardData.value(forKey: "GameIntoduction") as! String?
                     self.teamMember = self.jsonDashBoardData.value(forKey: "TeamMembers") as! NSDictionary
                    let value = self.teamMember.value(forKey: "GroupName") as! String
                    self.groupName.text = value
                    }

                }
                catch let error as NSError
                {
                    print(error)
                }
            }
        })
        task.resume()
    }
//redirect to simulationViewController page
    @IBAction func simulationShow(_ sender: Any) {
        DispatchQueue.main.async(){
            self.performSegue(withIdentifier: "simulation_select", sender:nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "simulation_select")
        {
            DispatchQueue.main.async(){
                _ = segue.destination
            }
        }
    }
    func switchIsChanged(Simulateswitch: UISwitch) {
        if Simulateswitch.isOn {
            simulationLabel.text = "Real Time"
        } else {
            simulationLabel.text = "Simulation"
        }
    }
//    func realTimeNotify(notification: Notification){
//        simulationSwitch.isOn = true
//        simulationSwitch.setOn(true, animated: false)
//        simulationSwitch.addTarget(self, action: #selector(self.switchIsChanged), for: .valueChanged)
//    }
    @IBAction func simulateSwitch(_ sender: Any) {
        if simulationSwitch.isOn {
            simulationLabel.text = "Real Time"
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RealTimeSwitch"), object: nil)
        } else {
            simulationLabel.text = "Simulation"
           NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SimulationSwitch"), object: nil)
        }
    }
}

