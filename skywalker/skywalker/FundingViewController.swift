//
//  FundingViewController.swift
//  skywalker
//
//  Created by Sabari on 2/15/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class FundingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var fundingTable: UITableView!
    
   
    var jsonFinancialFundingData = NSArray()
    var durationIdAmount = NSArray()
    var getValueBySection = [[Int]]()
    var gameFundingValue = [AnyObject]()
    var urlStrings = ApiUrl()
    override func viewDidLoad() {
        super.viewDidLoad()
        getFinancialFundingData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return jsonFinancialFundingData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (durationIdAmount[section] as AnyObject).count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.fundingTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
         cell.backgroundColor = UIColor.clear
        let label = cell.contentView.viewWithTag(1) as! UILabel
        let textField = cell.contentView.viewWithTag(2) as! UITextField
         let amountData = (durationIdAmount[indexPath.section] as AnyObject).value(forKey: "Amount") as! NSArray
        label.text="\(indexPath.row+1) Month"
        textField.text="\(amountData[indexPath.row])"
        print(amountData[indexPath.row])
        return cell
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let sectionValue = (jsonFinancialFundingData[section] as AnyObject).value(forKey: "Code") as! String
        return sectionValue

    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }
    /*!
     *@brief get financial funding data (financial funding data data) from server and reload the tableview to update the data
     */
    func getFinancialFundingData()
    {
        let urlValue = URL(string: urlStrings.getFinancialFunding)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonFinancialFundingData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                       self.durationIdAmount = self.jsonFinancialFundingData.value(forKey: "durationIdAmount") as! NSArray
                        print(self.jsonFinancialFundingData)
                        DispatchQueue.main.async(){
                            self.fundingTable.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    @IBAction func saveButton(_ sender: Any) {
        
        var getValue = [Int]()
        var getelement = [String : Any]()
        var fundingJsonarr = [Any]()
        //get values from each section and row
        //i(section) j(row) store value in array base on section
        for i in 0..<self.jsonFinancialFundingData.count
        {
            for j in 0..<(self.durationIdAmount[i] as AnyObject).count
            {
                let indexPath = IndexPath(row: j, section: i)
                let cell: UITableViewCell? = fundingTable.cellForRow(at: indexPath)!
                let textField = cell!.contentView.viewWithTag(2) as! UITextField
                let textValue = textField.text!
                getValue.append(Int(textValue)!)

            }
            getValueBySection.append(getValue)
            getValue.removeAll()
        }

        print(getValueBySection)
        
        //construct json data from tableview(section and row)
        for i in 0..<self.jsonFinancialFundingData.count
        {
            for j in 0..<(self.durationIdAmount[i] as AnyObject).count
            {
                let gameFundingAmount = (self.durationIdAmount[i] as AnyObject).value(forKey: "Amount") as! NSArray
                let gameFundingDuration = (self.durationIdAmount[i] as AnyObject).value(forKey: "DurationId") as! NSArray
                let gameVariable = ["Amount":gameFundingAmount[j],"DurationId":gameFundingDuration[j]] as [String : Any]
                gameFundingValue.append(gameVariable as AnyObject)
            }
            getelement = ["CurrencyId":(jsonFinancialFundingData[i] as AnyObject).value(forKey: "CurrencyId")!,"durationIdAmount":gameFundingValue] as [String : Any]
            gameFundingValue.removeAll()
            fundingJsonarr.append(getelement)
        }
        print(fundingJsonarr)
        
        //send json data to server
        let jsonData = try? JSONSerialization.data(withJSONObject: fundingJsonarr, options: .prettyPrinted)
        let decodedValue = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSArray
        
        let jsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
        let urlValue = URL(string: "http://aktrea.com/skywalker/api/Skywalker/SaveFunding")!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData1
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let PostResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(PostResponse)
                    }
                    
                }
                
        })
        
        task.resume()

    }

    @IBAction func commitButton(_ sender: Any) {
    }
    }
