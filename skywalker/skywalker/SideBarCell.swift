//
//  SideBarCell.swift
//  skywalker
//
//  Created by Sabari on 2/9/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class SideBarCell: UITableViewCell {

    @IBOutlet weak var tittle: UILabel!
    @IBOutlet weak var icon: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
