//
//  KeyMetricsViewController.swift
//  skywalker
//
//  Created by Sabari on 5/4/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class KeyMetricsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var urlStrings = ApiUrl()
    @IBOutlet weak var menuBar: UIBarButtonItem!
    @IBOutlet var keyTable: UITableView!
    var jsonKeyMetricsData = NSArray()
    var chart: PDChart!
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenuBarBtn(menuBar)
        self.getKeyMetrics()

    }

    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // menuBar.transitioningDelegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getKeyMetrics()
    {
        let urlValue = URL(string: urlStrings.keymetrics)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {(data, response, error) in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        self.jsonKeyMetricsData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSArray
                        print(self.jsonKeyMetricsData)
                        DispatchQueue.main.async()
                        {
                        self.keyTable.reloadData()
                        }
                        
                        
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
                
        })
        task.resume()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonKeyMetricsData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.keyTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let titleLabel = cell.contentView.viewWithTag(2) as! UILabel
        titleLabel.text = (self.jsonKeyMetricsData[indexPath.row] as AnyObject).value(forKey: "Name") as? String
        
        
        let stage = (self.jsonKeyMetricsData[indexPath.row] as AnyObject).value(forKey: "StageValue") as! NSArray
        var barValue = [CGPoint]()
        //var stagecount = stage?.count
            for i in 0..<stage.count
            {
                let yValue = (stage[i] as AnyObject).value(forKey: "Value")
                barValue.append(CGPoint(x: i, y: yValue as! Int))
            }
      
        print(barValue)
      //  let yMaximum = (stage[indexPath.row] as AnyObject).value(forKey: "Value") as! CGFloat
        let barView = cell.contentView.viewWithTag(1)! as UIView
        
        let dataItem: PDBarChartDataItem = PDBarChartDataItem()
        dataItem.xMax = CGFloat(stage.count)
        dataItem.xInterval = 1.0
        dataItem.yMax = 500000.0
            dataItem.yInterval = 100000.0
        dataItem.axesTipColor = UIColor.white
        dataItem.barPointArray = barValue
        dataItem.xAxesDegreeTexts = ((stage as AnyObject).value(forKey: "StageName") as! NSArray) as? [String]
        //dataItem.yAxesDegreeTexts = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
        
        let frameW = barView.frame.size.width
        let barChart: PDBarChart = PDBarChart(frame: CGRect(x: 0, y: 50, width: frameW, height: frameW), dataItem: dataItem)
        barChart.strokeChart()
        barChart.backgroundColor=UIColor.clear
        barView.addSubview(barChart)
        
        return cell

    }
}
