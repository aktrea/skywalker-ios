//
//  ReportViewController.swift
//  skywalker
//
//  Created by Sabari on 2/24/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class ReportViewController: UIViewController,UITableViewDataSource,CZPickerViewDelegate, CZPickerViewDataSource {
    struct Section {
        var name: String!
        var items: [String]!
        var collapsed: Bool!
        
        init(name: String, items: [String], collapsed: Bool = false) {
            self.name = name
            self.items = items
            self.collapsed = collapsed
        }
    }
    var jsonLocationData=NSArray()
    var jsonReportData = NSArray()
    var jsonFinacialData = NSArray()
    var tableData = NSArray()
    var jsonMarketData = NSArray()
    var jsonPeopleData = NSArray()
    var jsonAwardData = NSArray()
    var stageValues = [String]()
    var sections = [Section]()
    var stageValueRow = [String]()
    var picker = CZPickerView()
    var urlStrings = ApiUrl()
    var locationName = [String]()
    var locationId = [Int]()
    var locationValue = Int()
    
    @IBOutlet weak var menuBar: UIBarButtonItem!
    @IBOutlet weak var financialReportTable: UITableView!
    @IBOutlet weak var marketTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setMenuBarBtn(menuBar)
        self.getReport()
        self.getLocationData()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func reportSegment(_ sender: Any) {
        switch (sender as AnyObject).selectedSegmentIndex
        {
        case 0:
            sections.removeAll()
            tableData = self.jsonFinacialData
            reloadData(getData: tableData)
        case 1:
            sections.removeAll()
            tableData = self.jsonMarketData
            reloadData(getData: tableData)
        case 2:
            sections.removeAll()
            tableData = self.jsonPeopleData
            reloadData(getData: tableData)
        case 3:
            sections.removeAll()
            tableData = self.jsonAwardData
            reloadData(getData: tableData)
        default:
            break;
        }
    }
    func reloadData(getData:NSArray)
    {
        for i in 0..<getData.count
        {
            
            let values = (getData[i] as AnyObject).value(forKey: "Variables") as! NSArray
            
            for j in 0..<values.count
            {
                let stage = (values[j] as AnyObject).value(forKey: "StageReportValues") as! NSArray
                for k in 0..<stage.count
                {
                    let stageKey = (stage[k] as AnyObject).value(forKey: "StageName")!
                    let valueKey = (stage[k] as AnyObject).value(forKey: "Value")!
                    let stageAndValue = "\(stageKey): \(valueKey)"
                    self.stageValues.append(stageAndValue)
                    
                }
                if ((values[j] as AnyObject).value(forKey: "Name") is NSNull )
                {
                    let substring = (values[j] as AnyObject).value(forKey: "SubTitle") as! NSArray
                    for ii in 0..<substring.count
                    {
                        let values = (substring[ii] as AnyObject).value(forKey: "Variables") as! NSArray
                        for jj in 0..<values.count
                        {
                            let stage = (values[jj] as AnyObject).value(forKey: "StageReportValues") as! NSArray
                            for kk in 0..<stage.count
                            {
                                let stageKey = (stage[kk] as AnyObject).value(forKey: "StageName")!
                                let valueKey = (stage[kk] as AnyObject).value(forKey: "Value")!
                                let stageAndValue = "\(stageKey): \(valueKey)"
                                self.stageValues.append(stageAndValue)
                                
                            }
                            let names = (values[jj] as AnyObject).value(forKey: "Name") as! String
                            let title = (substring[ii] as AnyObject).value(forKey: "SubTitleString") as! String
                            let cleanTitle = title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                            let titleAndName = "(\(cleanTitle))-\(names)"
                            self.sections.append(Section(name: titleAndName, items: self.stageValues))
                            
                            self.stageValues.removeAll()
                            
                        }
                    }
                    
                }
                else
                {
                    let names = (values[j] as AnyObject).value(forKey: "Name") as! String
                    let title = (getData[i] as AnyObject).value(forKey: "TitleString") as! String
                    let cleanTitle = title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    let titleAndName = "(\(cleanTitle))-\(names)"
                    self.sections.append(Section(name: titleAndName, items: self.stageValues))
                    self.stageValues.removeAll()
                }
            }
        }
        print(self.sections)
        DispatchQueue.main.async()
            {
               
                self.tableData = self.jsonFinacialData
                self.reloadData(getData: self.tableData)
        }
        
    }
    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // menuBar.transitioningDelegate = self
        
    }
    
    func getReport()
    {
        let urlValue = URL(string: "\(urlStrings.report)\(locationValue)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {(data, response, error) in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        self.jsonReportData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSArray
                        //print(self.jsonReportData)
                        self.jsonFinacialData = (self.jsonReportData[0] as AnyObject).value(forKey: "Titles") as! NSArray
                        self.jsonMarketData = (self.jsonReportData[1] as AnyObject).value(forKey: "Titles") as! NSArray
                        self.jsonPeopleData = (self.jsonReportData[2] as AnyObject).value(forKey: "Titles") as! NSArray
                        self.jsonAwardData = (self.jsonReportData[3] as AnyObject).value(forKey: "Titles") as! NSArray
                        print(self.jsonFinacialData)
                        
                        
                        
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
                
        })
        task.resume()
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        // For section 1, the total count is items count plus the number of headers
        var count = sections.count
        
        for section in sections {
            count += section.items.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Calculate the real section index and row index
        let section = getSectionIndex(indexPath.row)
        let row = getRowIndex(indexPath.row)
        
        if row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! ReportTableViewCell
            cell.titleLabel.text = sections[section].name
            cell.toggleButton.tag = section
            cell.toggleButton.setTitle(sections[section].collapsed! ? "" : "", for: UIControlState())
            cell.toggleButton.addTarget(self, action: #selector(ReportViewController.toggleCollapse), for: .touchUpInside)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as UITableViewCell!
            let label = cell?.contentView.viewWithTag(2) as! UILabel
            label.text = sections[section].items[row - 1]
            return cell!
        }
    }
    
    //
    // MARK: - Event Handlers
    //
    func toggleCollapse(_ sender: UIButton) {
        let section = sender.tag
        let collapsed = sections[section].collapsed
        
        // Toggle collapse
        sections[section].collapsed = !collapsed!
        
        let indices = getHeaderIndices()
        
        let start = indices[section]
        let end = start + sections[section].items.count
        
        marketTable.beginUpdates()
        for i in start ..< end + 1 {
            marketTable.reloadRows(at: [IndexPath(row: i, section: sender.tag)], with: .automatic)
        }
        marketTable.endUpdates()
    }
    
    //
    // MARK: - Helper Functions
    //
    func getSectionIndex(_ row: NSInteger) -> Int {
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        
        return -1
    }
    
    func getRowIndex(_ row: NSInteger) -> Int {
        var index = row
        let indices = getHeaderIndices()
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        
        return index
    }
    
    func getHeaderIndices() -> [Int] {
        var index = 0
        var indices: [Int] = []
        
        for section in sections {
            indices.append(index)
            index += section.items.count + 1
        }
        
        return indices
    }
    
    @IBAction func locationButton(_ sender: Any) {
        picker = CZPickerView(headerTitle: "countryName", cancelButtonTitle: "Cancel", confirmButtonTitle: "Confirm")
        picker.delegate = self
        picker.dataSource = self
        picker.needFooterView = false
        picker.show()
        
    }
    func numberOfRows(in pickerView: CZPickerView!) -> Int {
        return self.locationName.count
    }
    func czpickerView(_ pickerView: CZPickerView!, titleForRow row: Int) -> String! {
        return self.locationName[row] as String!
    }
    func czpickerView(_ pickerView: CZPickerView!, didConfirmWithItemAtRow row: Int){
        print(self.locationId[row])
        locationValue = self.locationId[row] as! Int
    }
    func czpickerViewDidClickCancelButton(_ pickerView: CZPickerView!) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    /*!
     *@brief get all location data from server and reload the tableview to update the data
     */
    func getLocationData()
    {
        let urlValue = URL(string: urlStrings.getAllLocations)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonLocationData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        
                        self.locationName = (self.jsonLocationData.value(forKey: "Name") as! NSArray) as! [String]
                        self.locationId = (self.jsonLocationData.value(forKey: "Id") as! NSArray) as! [Int]
                        
                        DispatchQueue.main.async(){
                            // self.locationCollection.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    
}
