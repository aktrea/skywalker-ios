//
//  ScoreViewController.swift
//  skywalker
//
//  Created by Sabari on 30/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var scoreTable: UITableView!
    @IBOutlet var leaderTable: UITableView!
    var session : URLSession!
    var task : URLSessionDataTask!
    var urlStrings = ApiUrl()
    var jsonScoreData : NSDictionary = [:]
    var awardData : NSArray = []
    var overallData : NSArray = []
    var tableData :NSArray = []
    var segmentValue = Bool()
    @IBOutlet var menuBar: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone)
        {
        setMenuBarBtn(menuBar)
        }
        session = URLSession.shared
        task = URLSessionDataTask()
        getScoreData()
        self.scoreTable.isHidden = false
        self.leaderTable.isHidden = true
        segmentValue = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // menuBar.transitioningDelegate = self
        
    }
    
    func getScoreData()
    {
        let urlValue = URL(string: urlStrings.scoreData)!
        task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print(error!.localizedDescription)
            }
            else
            {
                do
                {
                    self.jsonScoreData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                    self.awardData = self.jsonScoreData.value(forKey: "Awards") as! NSArray
                    self.overallData = self.jsonScoreData.value(forKey: "OverAllPerformance") as! NSArray
                    self.tableData = self.awardData
                    print(self.jsonScoreData)
                    DispatchQueue.main.async()
                        {
                    self.scoreTable.reloadData()
                    self.leaderTable.reloadData()
                    }
                    
                }
                catch
                {
                    print(error.localizedDescription)
                }
            }
        })
        task.resume()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == scoreTable)
        {
        return 1
        }
        if(tableView == leaderTable)
        {
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == scoreTable)
        {
            return tableData.count
        }
        if(tableView == leaderTable)
        {
        return tableData.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell0 = UITableViewCell()
        if(tableView == scoreTable)
        {
            // let cell11 = self.scoreTable.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ScoreTableViewCell1
            let score = (tableData[indexPath.row] as AnyObject).value(forKey: "Score") as! Int
            let rank = (tableData[indexPath.row] as AnyObject).value(forKey: "Rank") as! Int
            if(indexPath.row % 2 == 0)
            {
   
        let cell = self.scoreTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScoreTableViewCell
        cell.backgroundColor=UIColor.clear
        cell.rankValue.layer.cornerRadius = 8;
        cell.rankValue.layer.masksToBounds = true
        cell.rankValue.layer.borderColor = UIColor.white.cgColor
        cell.rankValue.layer.borderWidth = 1
        
        cell.titleName.text = (tableData[indexPath.row] as AnyObject).value(forKey: "Award") as? String
        cell.scoreValue.text = String(score)
        cell.rankValue.text = String(rank)
        return cell
            }
            else
            {
                let cell11 = self.scoreTable.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! ScoreTableViewCell1
                cell11.backgroundColor=UIColor.clear
                cell11.rankValue.layer.cornerRadius = 8;
                cell11.rankValue.layer.masksToBounds = true
                cell11.rankValue.layer.borderColor = UIColor.white.cgColor
                cell11.rankValue.layer.borderWidth = 1
                
                cell11.titleName.text = (tableData[indexPath.row] as AnyObject).value(forKey: "Award") as? String
                cell11.scoreValue.text = String(score)
                cell11.rankValue.text = String(rank)
                return cell11

            }
        }
        if(tableView == leaderTable)
        {
            let score = (tableData[indexPath.row] as AnyObject).value(forKey: "Score") as! Int
            let rank = (tableData[indexPath.row] as AnyObject).value(forKey: "Rank") as! Int

            if(indexPath.row % 2 == 0)
            {
                let cell1 = self.leaderTable.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! LeaderTableViewCell
                cell1.view1.layer.cornerRadius = 20;
                cell1.view1.layer.masksToBounds = true
                cell1.backgroundColor = UIColor.clear
               // cell1.view1.layer.borderColor = UIColor.white.cgColor
                //cell1.view1.layer.borderWidth = 1

                cell1.teamName.text = (tableData[indexPath.row] as AnyObject).value(forKey: "TeamName") as? String
                cell1.Score.text = String("Score:\(score)")
                cell1.rankValue.text = String(rank)
                return cell1
            }
            else
            {
                let cell2 = self.leaderTable.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! Leader1TableViewCell
                cell2.view2.layer.cornerRadius = 20;
                cell2.view2.layer.masksToBounds = true
                cell2.backgroundColor=UIColor.clear
                //cell2.view2.layer.borderColor = UIColor.white.cgColor
               // cell2.view2.layer.borderWidth = 1

                cell2.teamName.text = (tableData[indexPath.row] as AnyObject).value(forKey: "TeamName") as? String
                cell2.Score.text = String("Score:\(score)")
                cell2.rankValue.text = String(rank)
                return cell2
            }
            
        }
        return cell0
    }
    @IBAction func segment(_ sender: Any) {
        
        switch (sender as AnyObject).selectedSegmentIndex {
        case 0:
            segmentValue = true
            self.tableData = self.awardData
            self.leaderTable.isHidden = true
            self.scoreTable.isHidden = false
            self.scoreTable.reloadData()
            break
            
        case 1:
            self.tableData = self.overallData
            self.scoreTable.isHidden = true
             self.leaderTable.isHidden = false
            self.leaderTable.reloadData()
            break
            
        default:
            break
        }
    }
}
