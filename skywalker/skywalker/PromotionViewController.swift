//
//  PromotionViewController.swift
//  skywalker
//
//  Created by Sabari on 2/13/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class PromotionViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var promotionTable: UITableView!
    var jsonPromotionData = NSArray()
    var playerValueGameVariable = NSArray()
    var locationId = [AnyObject]()
    var selectGameVariableId = [Int]()
    var gameVariableIdValue = [AnyObject]()
    var indexRow = [Int]()
    var indexSection = [Int]()
    var checkStatus = Int()
    var urlString = String()
    var checkStatusCount = Int()
    var checkIntialState = Int()
    var urlStrings = ApiUrl()
    var segment = ViewController()
    var getValue = [[Bool]]()
    var simulateValue = String()
    var getSteps = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String
        checkStatusCount = 0
        checkIntialState = 0
        self.commitButton.layer.cornerRadius=5
        self.commitButton.layer.masksToBounds=true
        self.saveButton.layer.cornerRadius=5
        self.saveButton.layer.masksToBounds=true
        self.promotionTable.backgroundColor=UIColor.clear
        
        NotificationCenter.default.addObserver(self, selector: #selector(PromotionViewController.segmentChange(notification:)), name: Notification.Name("RedirectSegment"), object: nil)
        // Do any additional setup after loading the view.
        
        
        
        for i in 0..<SegmentViewController.RealTime.realtimeTabArray.count
        {
            if(SegmentViewController.RealTime.realtimeTabArray[i] == "CHANNEL")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
                getSteps = "CHANNEL"
                break
            }
            else
            {
                self.saveButton.isUserInteractionEnabled = false
                self.commitButton.isUserInteractionEnabled = false
                getSteps = ""
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkIsCommit()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return jsonPromotionData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (playerValueGameVariable[section] as AnyObject).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.promotionTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        let label = cell.contentView.viewWithTag(1) as! UILabel
        label.textAlignment=NSTextAlignment.center
        let gameVariableName = (playerValueGameVariable[indexPath.section] as AnyObject).value(forKey: "GameVariableName") as! NSArray
        //if selected tick will appear based on isSelect
        if(self.getValue[indexPath.section][indexPath.row])
        {
            label.textColor = UIColor.black
            label.layer.cornerRadius=5
            label.layer.masksToBounds=true
            label.backgroundColor=UIColor.white
            cell.accessoryType = .none
            
        }
        else
        {
            cell.accessoryType = .none
            label.textColor = UIColor.white
            label.layer.cornerRadius=5
            label.layer.masksToBounds=true
            label.layer.borderWidth=1
            label.layer.borderColor=UIColor.white.cgColor
            label.backgroundColor=UIColor.clear
            
        }
        label.text = gameVariableName[indexPath.row] as? String
        return cell
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if(getSteps == "CHANNEL" || simulateValue != "RealTime")
        {
            if(checkStatus == 2)
            {
                if let cell = tableView.cellForRow(at: indexPath) {
                    
                    cell.tintColor = UIColor.white
                    let label = cell.contentView.viewWithTag(1) as! UILabel
                    label.textColor = UIColor.white
                    cell.accessoryType = .none
                    cell.backgroundColor=UIColor.clear
                    label.backgroundColor=UIColor.clear
                    label.layer.cornerRadius=5
                    label.layer.masksToBounds=true
                    label.layer.borderWidth=1
                    label.layer.borderColor=UIColor.white.cgColor
                    getValue[indexPath.section][indexPath.row] = false
                }
            }
        }
        else
        {
            Util.showNotification(title: "",subTitle: urlStrings.bannerMessage ,color: Util.BannerColors.blue)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(getSteps == "CHANNEL" || simulateValue != "RealTime")
        {
            if(checkStatus == 2)
            {
                let gameVariableid = (playerValueGameVariable[indexPath.section] as AnyObject).value(forKey: "GameVariableId") as! NSArray
                selectGameVariableId.append(gameVariableid[indexPath.row] as! Int)
                if let cell = tableView.cellForRow(at: indexPath) {
                    let label = cell.contentView.viewWithTag(1) as! UILabel
                    label.textColor = UIColor.black
                    label.layer.cornerRadius=5
                    label.layer.masksToBounds=true
                    indexRow.append(indexPath.row)
                    indexSection.append(indexPath.section)
                    label.backgroundColor=UIColor.white
                    getValue[indexPath.section][indexPath.row] = true
                    cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.layer.transform = CATransform3DMakeScale(1.05, 1.05, 1)
                    },completion: { finished in
                        UIView.animate(withDuration: 0.1, animations: {
                            cell.layer.transform = CATransform3DMakeScale(1, 1, 1)
                        })
                    })
                }
            }
        }
        else
        {
            Util.showNotification(title: "",subTitle: urlStrings.bannerMessage ,color: Util.BannerColors.blue)
        }
        
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let sectionValue = (jsonPromotionData[section] as AnyObject).value(forKey: "LocationName") as! String
        return sectionValue
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    /*!
     *@brief get GameVariable data (promotion data)from server and reload the tableview to update the data
     */
    func getGameVariableData()
    {
        let url = urlStrings.getPlayerLocationIdandGameVariableId
        let urlValue = URL(string: "\(url)\(3)")!//stepid =3
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        //get responce data for promotion page
                        self.jsonPromotionData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        print(self.jsonPromotionData)
                        self.playerValueGameVariable = self.jsonPromotionData.value(forKey: "playerValueGameVariable") as! NSArray
                        
                        
                        //get location id and stored in array to access tableview
                        for i in 0..<self.jsonPromotionData.count
                        {
                            let gameValue = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "IsSelected") as! NSArray
                            var valueArray = [Bool]()
                            
                            let locationIdValue = (self.jsonPromotionData[i] as AnyObject).value(forKey: "locationId") as! Int
                            
                            self.locationId.append(locationIdValue as AnyObject)
                            
                            
                            //get selected index value from responce json data
                            for j in 0..<(self.playerValueGameVariable[i] as AnyObject).count
                            {
                                valueArray.append(gameValue[j] as! Bool)
                                let isSelected = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "IsSelected") as! NSArray
                                let state = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "CheckState") as! NSArray
                                if(isSelected[j]) as! Bool
                                {
                                    self.indexSection.append(i)
                                    self.indexRow.append(j)
                                    //for new user Is selected is 0 this loop is not execute for new user
                                    self.checkIntialState = 1
                                }
                                if(isSelected[j] as! Bool == true)
                                {
                                    if(state[j] as! Int != 0)
                                    {
                                        self.checkStatusCount = 1;
                                        self.checkStatus = state[j] as! Int
                                    }
                                }
                            }
                            self.getValue.append(valueArray as [Bool])
                        }
                        if(self.checkStatusCount == 0)
                        {
                            self.checkStatus = 0
                        }
                        //In First time(new user) check State is 0.
                        //if checkstatus = 2 save button intraction isenable
                        //set checkStatus =2
                        //now savebutton will be enable at new user
                        if(self.checkIntialState == 0)
                        {
                            self.checkStatus = 2
                        }
                        if(self.checkStatus == 0)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = false
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 2)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.saveButton.layer.borderColor = UIColor.white.cgColor
                                    self.saveButton.layer.borderWidth=1
                                    self.commitButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = true
                                    self.commitButton.isUserInteractionEnabled=false
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 1)
                        {
                            DispatchQueue.main.async()
                                {
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.isHidden=true
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Revoke",for: .normal)
                            }
                            
                        }
                        
                        DispatchQueue.main.async(){
                            
                            //check for step read access
                            self.stepCheck()
                            self.promotionTable.reloadData()
                            
                            
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    
    @IBAction func saveButton(_ sender: Any) {
        urlString = urlStrings.savePlayerGameVariable
        savePromotion()
        //send notification when user in realtime simulation
        if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Channel Saved"])
        }
    }
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var commitButton: UIButton!
    @IBAction func commitButton(_ sender: Any) {
        if(self.checkStatus == 1)
        {
            urlString = urlStrings.revokePlayerGameVariable
            savePromotion()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Channel Revoked"])
            }
        }
        else  if(self.checkStatus == 0)
        {
            urlString = urlStrings.submitPlayerGameVariable
            savePromotion()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Channel Commited"])
            }
        }
        
    }
    //remove duplicate element from array function
    func uniq<S : Sequence, T : Hashable>(source: S) -> [T] where S.Iterator.Element == T {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source
        {
            if !added.contains(elem)
            {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
    func savePromotion()
    {
        var getelement = [String : Any]()
        var promoptionJsonarr = [Any]()
        
        
        for i in 0..<self.getValue.count
        {
            for j in 0..<self.getValue[i].count
            {
                if(self.getValue[i][j] == true)
                {
                    let gameVariableid = (playerValueGameVariable[i] as AnyObject).value(forKey: "GameVariableId") as! NSArray
                    let gameVariable = ["GameVariableId":gameVariableid[j],"Value":0] as [String : Any]
                    gameVariableIdValue.append(gameVariable as AnyObject)
                    
                }
            }
            getelement = ["LocationId":locationId[i],"stepId":"3","GameVariableValue":gameVariableIdValue] as [String : Any]
            gameVariableIdValue.removeAll()
            promoptionJsonarr.append(getelement)
            
        }
        
        
        print(promoptionJsonarr)
        //send json data to server
        let jsonData = try? JSONSerialization.data(withJSONObject: promoptionJsonarr, options: .prettyPrinted)
        let decodedValue = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSArray
        
        let jsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
        let urlValue = URL(string: urlString)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData1
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let PostResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(PostResponse)
                        if(self.checkStatus == 0)
                        {
                            let userId = UserDefaults.standard
                            let segVal = userId.value(forKey: "segmentVal") as! Int
                            userId.set(segVal+1, forKey: "segmentVal")
                            userId.synchronize()
                            let sendSegmentValue = ["SegmentValue":userId.value(forKey: "segmentVal")]
                            print(sendSegmentValue)
                            NotificationCenter.default.post(name: Notification.Name("RedirectSegment"), object:nil,userInfo:sendSegmentValue)
                        }
                        
                        //after save checkStatus set to 0
                        if(self.checkStatus == 2)
                        {
                            if(PostResponse == "true")
                            {
                                //remove all element to avoid dupicate
                                self.indexSection.removeAll()
                                self.indexRow.removeAll()
                                self.checkStatus=0
                                self.getGameVariableData()
                                self.locationId.removeAll()
                                self.getValue.removeAll()
                            }
                        }
                        else
                        {
                            if(PostResponse == "true")
                            {
                                //remove all element to avoid dupicate
                                self.indexSection.removeAll()
                                self.indexRow.removeAll()
                                self.locationId.removeAll()
                                self.getGameVariableData()
                                self.getValue.removeAll()
                            }
                        }
                        
                    }
                    
                }
                
        })
        
        task.resume()
        
    }
    func hideKeyboard() {
        promotionTable.endEditing(true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        animateCell(cell: cell)
    }
    
    func animateCell(cell: UITableViewCell) {
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.fromValue = 200
        cell.layer.cornerRadius = 0
        animation.toValue = 0
        animation.duration = 1
        cell.layer.add(animation, forKey: animation.keyPath)
    }
    
    func checkIsCommit()
    {
        let url = urlStrings.isCommit
        let urlValue = URL(string: "\(url)\(3)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                    print(postResponse)
                    if(postResponse == "true")
                    {
                        let userId = UserDefaults.standard
                        let segVal = userId.value(forKey: "segmentVal") as! Int
                        userId.set(segVal+1, forKey: "segmentVal")
                        userId.synchronize()
                        self.indexSection.removeAll()
                        self.indexRow.removeAll()
                        self.locationId.removeAll()
                        self.getGameVariableData()
                    }
                    else
                    {
                        
                    }
                    
                    
                }
            }
        })
        task.resume()
    }
    func segmentChange(notification: Notification)
    {
        self.viewWillAppear(true)
    }
    //check for step read access
    func stepCheck()
    {
        
        if(simulateValue == "RealTime")
        {
            if(getSteps == "CHANNEL")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
            }
            else
            {
                self.saveButton.isHidden = true
                self.commitButton.isHidden = true
            }
        }
    }
    
}
