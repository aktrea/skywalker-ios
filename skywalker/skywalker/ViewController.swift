//
//  ViewController.swift
//  skywalker
//
//  Created by Sabari on 2/9/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,MXSegmentedPagerDelegate {
    
    
    var jsonLocationData=NSArray()
    var jsonPlayerLocationData=NSArray()
    var locationName = [String]()
    var homeLocation = [Bool]()
    var selectLocation = [String]()
    var selectHomeLocation = [Bool]()
    var userSelectedLocation = [Int]()
    var locationId = [Int]()
    var playerValueStatus = Int()
    var urlString = String()
    var urlStrings = ApiUrl()
    var reloadProduct = Int()
    var segmentPage = SegmentViewController()
    var segmentValue = Int()
    var homeLocationName = [String]()
    var homeLocationId = [Int]()
    var homeLocationValue = [Bool]()
    var cell = LocationCollectionViewCell()
    var simulateValue = String()
    var getSteps = String()
    
    
    
    @IBOutlet weak var locationCollection: UICollectionView!
    @IBOutlet weak var locationSelectCollection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String
        let userId = UserDefaults.standard
        userId.set(1, forKey: "segmentVal")
        userId.synchronize()
        self.getLocationData()
        segmentValue = 0
        locationCollection.layer.borderColor = UIColor.white.cgColor
        locationCollection.layer.borderWidth = 0.7
        locationCollection.layer.cornerRadius = 5
        locationCollection.layer.masksToBounds = true
        
        locationSelectCollection.layer.borderColor = UIColor.white.cgColor;
        locationSelectCollection.layer.borderWidth = 0.7
        locationSelectCollection.layer.cornerRadius = 5
        locationSelectCollection.layer.masksToBounds = true
        
        
        
        
        for i in 0..<SegmentViewController.RealTime.realtimeTabArray.count
        {
            if(SegmentViewController.RealTime.realtimeTabArray[i] == "LOCATION")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
                self.cell.isUserInteractionEnabled = true
                getSteps = "LOCATION"
                break
            }
            else
            {
                self.saveButton.isUserInteractionEnabled = false
                self.commitButton.isUserInteractionEnabled = false
                self.cell.isUserInteractionEnabled = false
                getSteps = ""
            }
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func setMenuBarBtn(_ menuBar: UIBarButtonItem) {
        menuBar.target = revealViewController()
        menuBar.action = #selector(SWRevealViewController.revealToggle(_:))
        view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        // menuBar.transitioningDelegate = self
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*!
     *@brief get all location data from server and reload the tableview to update the data
     */
    func getLocationData()
    {
        let urlValue = URL(string: urlStrings.getAllLocations)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonLocationData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        print(self.jsonLocationData)
                        self.locationName = (self.jsonLocationData.value(forKey: "Name") as! NSArray) as! [String]
                        self.locationId = (self.jsonLocationData.value(forKey: "Id") as! NSArray) as! [Int]
                        self.homeLocation = (self.jsonLocationData.value(forKey: "IsHomeLocation") as! NSArray) as! [Bool]
                        
                        self.getPlayerLocationData()
                        
                        DispatchQueue.main.async(){
                            // self.locationCollection.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    /*!
     *@brief get all location data from server and reload the tableview to update the data
     */
    func getPlayerLocationData()
    {
        let urlValue = URL(string: urlStrings.getPlayerLocations)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonPlayerLocationData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as AnyObject as! NSArray
                        if(self.jsonPlayerLocationData.count != 0)
                        {
                            self.playerValueStatus = (self.jsonPlayerLocationData[0] as AnyObject).value(forKey: "PlayerValueStatus") as! Int
                            print(self.playerValueStatus)
                            if(self.playerValueStatus == 2)
                            {
                                DispatchQueue.main.async()
                                    {
                                        //set border for enabled button
                                        self.saveButton.layer.borderColor = UIColor.white.cgColor
                                        self.saveButton.layer.borderWidth=1
                                        self.commitButton.layer.borderWidth=0
                                        self.saveButton.isHidden=false
                                        self.saveButton.isUserInteractionEnabled = true
                                        self.commitButton.isUserInteractionEnabled=false
                                        self.commitButton.setTitle("Commit",for: .normal)
                                }
                            }
                            else  if(self.playerValueStatus == 0)
                            {
                                DispatchQueue.main.async()
                                    {
                                        //set border for enabled button
                                        self.commitButton.layer.borderColor = UIColor.white.cgColor
                                        self.commitButton.layer.borderWidth=1
                                        self.saveButton.layer.borderWidth=0
                                        self.saveButton.isHidden=false
                                        self.saveButton.isUserInteractionEnabled = false
                                        self.commitButton.isUserInteractionEnabled=true
                                        self.commitButton.setTitle("Commit",for: .normal)
                                }
                            }
                            else  if(self.playerValueStatus == 1)
                            {
                                DispatchQueue.main.async()
                                    {
                                        //set border for enabled button
                                        self.commitButton.layer.borderColor = UIColor.white.cgColor
                                        self.commitButton.layer.borderWidth=1
                                        self.saveButton.isHidden=true
                                        self.commitButton.isUserInteractionEnabled=true
                                        self.commitButton.setTitle("Revoke",for: .normal)
                                }
                                
                            }
                            let selectedLocation = self.jsonPlayerLocationData.value(forKey: "Locations") as! NSArray
                            print(selectedLocation)
                            self.selectLocation = selectedLocation.value(forKey: "Name") as! Array
                            self.userSelectedLocation = selectedLocation.value(forKey: "Id") as! Array
                            self.selectHomeLocation = selectedLocation.value(forKey: "IsHomeLocation") as! Array
                            //remove duplicate locationName,locationId,homelocation value
                            for loc in self.selectLocation {
                                if let locX = self.locationName.index(of: loc) {
                                    self.locationName.remove(at: locX)
                                    self.locationId.remove(at: locX)
                                    self.homeLocation.remove(at: locX)
                                }
                            }
                            
                            
                            DispatchQueue.main.async(){
                                self.locationSelectCollection.reloadData()
                                self.locationCollection.reloadData()
                                self.stepCheck()
                            }
                        }
                        else{
                            for i in 0..<self.homeLocation.count
                            {
                                if(self.homeLocation[i]==true)
                                {
                                    self.selectLocation.append(self.locationName[i])
                                    self.userSelectedLocation.append(self.locationId[i])
                                    self.selectHomeLocation.append(self.homeLocation[i])
                                }
                            }
                            //remove duplicate locationName,locationId,homelocation value
                            for loc in self.selectLocation {
                                if let locX = self.locationName.index(of: loc) {
                                    self.locationName.remove(at: locX)
                                    self.locationId.remove(at: locX)
                                    self.homeLocation.remove(at: locX)
                                    
                                }
                            }
                            //
                            DispatchQueue.main.async()
                                {
                                    self.saveButton.layer.borderColor = UIColor.white.cgColor
                                    self.saveButton.layer.borderWidth=1
                                    self.commitButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = true
                                    self.commitButton.isUserInteractionEnabled=false
                                    self.commitButton.setTitle("Commit",for: .normal)
                                    self.playerValueStatus = 2
                                    self.locationSelectCollection.reloadData()
                                    self.locationCollection.reloadData()
                                    self.stepCheck()
                            }
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == locationCollection)
        {
            return self.locationName.count
        }
        if(collectionView == locationSelectCollection)
        {
            return self.selectLocation.count
        }
        else
        {
            return 0;
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! LocationCollectionViewCell
        if(collectionView == locationCollection)
        {
            cell.locationTitle.layer.cornerRadius=5
            cell.locationTitle.layer.masksToBounds=true
            cell.locationTitle!.text = locationName[indexPath.row]
            return cell
        }
        
        
        if(collectionView == locationSelectCollection)
        {
            cell.locationTitle.layer.borderColor=UIColor.white.cgColor
            cell.locationTitle.layer.borderWidth=1
            cell.locationTitle.layer.cornerRadius=5
            cell.locationTitle.layer.masksToBounds=true
            cell.locationTitle!.text = selectLocation[indexPath.row]
            return cell
        }
        else
        {
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(playerValueStatus)
        if(getSteps == "LOCATION" || simulateValue != "RealTime")
        {
            if(playerValueStatus == 2)
            {
                if(collectionView == locationCollection)
                {
                    selectLocation.append(locationName[indexPath.row])
                    selectHomeLocation.append(homeLocation[indexPath.row])
                    locationName.remove(at: indexPath.row)
                    homeLocation.remove(at: indexPath.row)
                    
                    self.locationCollection.reloadData()
                    self.locationSelectCollection.reloadData()
                }
                if(collectionView == locationSelectCollection)
                {
                    // allow selection for non home location
                    if(selectHomeLocation[indexPath.row]==false)
                    {
                        locationName.append(selectLocation[indexPath.row])
                        homeLocation.append(selectHomeLocation[indexPath.row])
                        selectLocation.remove(at: indexPath.row)
                        selectHomeLocation.remove(at: indexPath.row)
                        self.locationSelectCollection.reloadData()
                        self.locationCollection.reloadData()
                        
                    }
                }
                
            }
        }
        else
        {
            Util.showNotification(title: "",subTitle: urlStrings.bannerMessage ,color: Util.BannerColors.blue)
        }
        
    }
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var commitButton: UIButton!
    @IBAction func saveButton(_ sender: Any) {
        urlString = urlStrings.savePlayerLocations
        //if user intially save location ProductViewcontroller data reload for updated location
        reloadProduct = 1
        saveLocation()
        //send notification when user in realtime simulation
        if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Location Saved"])
        }
    }
    @IBAction func commitButton(_ sender: Any) {
        if(self.playerValueStatus == 1)
        {
            urlString = urlStrings.revokePlayerLocations
            saveLocation()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Location Revoked"])
            }
        }
        else  if(self.playerValueStatus == 0)
        {
            urlString = urlStrings.submitPlayerLocations
            saveLocation()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" Location Commited"])
            }
            
        }
    }
    /*!
     *@brief save location as arrary using json data. send jsondata in post as NSData
     */
    func saveLocation()
    {
        var getValue = [String]()
        
        //get selected location
        for i in 0..<self.selectLocation.count
        {
            let indexPath = IndexPath(row: i, section: 0)
            let cell = locationSelectCollection.cellForItem(at: indexPath) as! LocationCollectionViewCell
            getValue.append(cell.locationTitle.text!)
        }
        var selectedId = [Int]()
        //get selected location id base on selected location name
        for i in 0..<self.jsonLocationData.count
        {
            let value = self.jsonLocationData.value(forKey:"Name") as! NSArray
            let valueId = self.jsonLocationData.value(forKey:"Id") as! NSArray
            for j in 0..<getValue.count
            {
                if(getValue[j] == String(describing: value[i]))
                {
                    selectedId.append(valueId[i] as! Int)
                }
            }
        }
        print(selectedId)
        let locationData :[String:Any] = ["LocationId":selectedId,"stepId":"1"]
        //convert jsondata array to data
        let jsonData = try? JSONSerialization.data(withJSONObject: locationData, options: .prettyPrinted)
        let urlValue = URL(string: urlString)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(postResponse)
                        if(postResponse == "true")
                        {
                            if(self.playerValueStatus == 0)
                            {
                                self.segmentValue = 0
                                let userId = UserDefaults.standard
                                let sendSegmentValue = ["SegmentValue":userId.value(forKey: "segmentVal")]
                                print(sendSegmentValue)
                                NotificationCenter.default.post(name: Notification.Name("RedirectSegment"), object:nil,userInfo:sendSegmentValue)
                                
                                
                            }
                            self.getPlayerLocationData()
                            if(self.reloadProduct == 1)
                            {
                                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
                            }
                        }
                    }
                    
                }
                
        })
        
        task.resume()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        animateCell(cell: cell)
    }
    
    func animateCell(cell: UICollectionViewCell) {
        let animation = CABasicAnimation(keyPath: "cornerRadius")
        animation.fromValue = 200
        cell.layer.cornerRadius = 0
        animation.toValue = 0
        animation.duration = 1
        cell.layer.add(animation, forKey: animation.keyPath)
    }
    
    func animateCellAtIndexPath(indexPath: NSIndexPath) {
        
        guard let cell = locationSelectCollection.cellForItem(at: indexPath as IndexPath) else { return }
        animateCell(cell: cell)
    }
    //check for step read access
    func stepCheck()
    {
        
        if(simulateValue == "RealTime")
        {
            if(getSteps == "LOCATION")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
            }
            else
            {
                self.saveButton.isHidden = true
                self.commitButton.isHidden = true
            }
        }
    }
    
}

