//
//  ApiUrl.swift
//  skywalker
//
//  Created by Sabari on 2/28/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import Foundation
class ApiUrl
{
    //login
     let loginUrl : String = "http://aktrea.com/skywalker/api/LoginApi/"
    //location
    let getAllLocations : String = "http://aktrea.com/skywalker/api/Skywalker/GetAllLocations"
    let getPlayerLocations : String = "http://aktrea.com/skywalker/api/Skywalker/GetPlayerLocations"
    let savePlayerLocations : String = "http://aktrea.com/skywalker/api/Skywalker/SavePlayerlocations"
    let revokePlayerLocations : String = "http://aktrea.com/skywalker/api/Skywalker/RevokePlayerlocations"
    let submitPlayerLocations : String = "http://aktrea.com/skywalker/api/Skywalker/SubmitPlayerlocations"
    //product,pricing
    let getProductList : String = "http://aktrea.com/skywalker/api/Skywalker/GetProductList"
    let getPlayerLocationandProductId : String = "http://aktrea.com/skywalker/api/Skywalker/GetPlayerLocationandProductId?stepid=2"//stepid =2,step=8
    let savePlayerProduct : String = "http://aktrea.com/skywalker/api/Skywalker/SavePlayerProduct"//stepid =2,step=8
    let revokePlayerProduct : String = "http://aktrea.com/skywalker/api/Skywalker/RevokePlayerProduct"//stepid =2,step=8
    let submitPlayerProduct : String = "http://aktrea.com/skywalker/api/Skywalker/SubmitPlayerProduct"//stepid =2,step=8
    let saveProductFeature : String = "http://aktrea.com/skywalker/api/Skywalker/SaveProductandFeature"
    let getProducts : String = "http://aktrea.com/skywalker/api/Skywalker/GetProducts"
    let getFeaturesList_productid : String = "http://aktrea.com/skywalker/api/Skywalker/GetFeaturesList?productid="
    //promotion,channel,people,cost,Allocation
    let getPlayerLocationIdandGameVariableId : String = "http://aktrea.com/skywalker/api/Skywalker/GetPlayerLocationandGameVariable?stepid="//stepid=3,stepid=4,stepid=5,stepid=6
    let savePlayerGameVariable : String = "http://aktrea.com/skywalker/api/Skywalker/SavePlayerGameVariable"//stepid=3,stepid=4,stepid=5,stepid=6

    let revokePlayerGameVariable : String = "http://aktrea.com/skywalker/api/Skywalker/RevokePlayerGameVariable"//stepid=3,stepid=4,stepid=5,stepid=6

    let submitPlayerGameVariable : String = "http://aktrea.com/skywalker/api/Skywalker/SubmitPlayerGameVariable"//stepid=3,stepid=4,stepid=5,stepid=6
    //financial
  let getFinancialFunding : String = "http://aktrea.com/skywalker/api/Skywalker/GetFinancialFunding"
   let getPlayerFinancialFunds : String = "http://aktrea.com/skywalker/api/Skywalker/GetPlayerFinancialFunds"
    //allocation
   let getPlayerProductandGameVariable : String = "http://aktrea.com/skywalker/api/Skywalker/GetPlayerProductIdAndGameVariableId?stepid="
    
   let savePlayeProductVariable: String = "http://aktrea.com/skywalker/api/Skywalker/SavePlayerProductGameVariable"
    let revokePlayeProductVariable: String = "http://aktrea.com/skywalker/api/Skywalker/RevokePlayerProductGameVariable"
    let submitPlayeProductVariable: String = "http://aktrea.com/skywalker/api/Skywalker/SubmitPlayerProductGameVariable"
    
}
