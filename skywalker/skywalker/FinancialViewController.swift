//
//  FinancialViewController.swift
//  skywalker
//
//  Created by Sabari on 2/15/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class FinancialViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var fundingView: UIView!
    @IBOutlet weak var allocationView: UIView!
    @IBOutlet weak var loanView: UIView!
    @IBOutlet weak var loanTable: UITableView!
    var jsonLoanData = NSArray()
    var urlStrings = ApiUrl()
    //var durationIdAmount = NSArray()

    let title1 = ["Amount", "Currency", "Intrest Rate", "Due Date"]
    override func viewDidLoad() {
        super.viewDidLoad()
        getLoanData()
        fundingView.isHidden = false
        allocationView.isHidden = true
        loanView.isHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func loanButton(_ sender: Any) {
        loanView.isHidden = false
        fundingView.isHidden = true
        allocationView.isHidden = true
    }
    @IBAction func switchView(_ sender: Any) {
        
        switch (sender as AnyObject).selectedSegmentIndex
        {
        case 0:
            fundingView.isHidden = false
            allocationView.isHidden = true
            loanView.isHidden = true
        case 1:
            fundingView.isHidden = true
            allocationView.isHidden = false
            loanView.isHidden = true
        default:
            break;
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return jsonLoanData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return title1.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.loanTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let label = cell.contentView.viewWithTag(1) as! UILabel
         var amountValue = String()
        if indexPath.row==0
        {
            let amount = jsonLoanData.value(forKey: "Amount") as! NSArray
            let amountNumber = amount[indexPath.section] as! NSNumber
        amountValue = String(describing: amountNumber)
        }
        else if indexPath.row==1
        {
        amountValue = (jsonLoanData[indexPath.section] as AnyObject).value(forKey: "Code") as! String
        }
        else if indexPath.row==2
        {
            let intrest = jsonLoanData.value(forKey: "InterestRate") as! NSArray
            let intrestNumber = intrest[indexPath.section] as! NSNumber
       amountValue = String(describing: intrestNumber)
        }
        else if indexPath.row==3
        {
        let dateValue = (jsonLoanData[indexPath.section] as AnyObject).value(forKey: "DueDate") as! String
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            let dateObj = dateFormatter.date(from: dateValue)
            dateFormatter.dateFormat = "MM-dd-yyyy"
            amountValue = dateFormatter.string(from: dateObj!)
        }
        label.text="\(title1[indexPath.row]):  \(amountValue)"
        return cell
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let sectionValue = (jsonLoanData[section] as AnyObject).value(forKey: "Date") as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateObj = dateFormatter.date(from: sectionValue)
        dateFormatter.dateFormat = "MM-dd-yyyy"
        return dateFormatter.string(from: dateObj!)
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
    }

    /*!
     *@brief get loan detail data ( loan detail) from server and reload the tableview to update the data
     */
    func getLoanData()
    {
        let urlValue = URL(string: urlStrings.getPlayerFinancialFunds)!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.jsonLoanData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        DispatchQueue.main.async(){
                            self.loanTable.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }

    
}
