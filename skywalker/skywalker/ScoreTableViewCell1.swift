//
//  ScoreTableViewCell1.swift
//  skywalker
//
//  Created by Sabari on 11/4/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class ScoreTableViewCell1: UITableViewCell {

    @IBOutlet var rankValue: UILabel!
    @IBOutlet var rank: UILabel!
    @IBOutlet var titleName: UILabel!
    @IBOutlet var score: UILabel!
    @IBOutlet var scoreValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
