//
//  PeopleViewController.swift
//  skywalker
//
//  Created by Sabari on 2/14/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class PeopleViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    var allCellsText = [String]()
    var jsonPeopleData = NSArray()
    var playerValueGameVariable = NSArray()
    var getValueBySection = [[Int]]()
    var gameVariableIdValue = [AnyObject]()
    var checkStatus = Int()
    var urlString = String()
    var checkStatusCount = Int()
    var checkIntialState = Int()
    var row = Int()
    var section = Int()
    var urlStrings = ApiUrl()
    var segment = ViewController()
     var getValue = [[Int]]()
    var simulateValue = String()
    var getSteps = String()
    
    @IBOutlet weak var peopleTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        simulateValue = (UserDefaults.standard.value(forKey: "simulationSelect") as! NSString) as String
        checkStatusCount = 0
        checkIntialState = 0
        self.commitButton.layer.cornerRadius=5
        self.commitButton.layer.masksToBounds=true
        self.saveButton.layer.cornerRadius=5
        self.saveButton.layer.masksToBounds=true
        
        //recognize when user touch outside
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AllocationViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        peopleTable.addGestureRecognizer(tapGesture)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(PeopleViewController.segmentChange(notification:)), name: Notification.Name("RedirectSegment"), object: nil)
        // Do any additional setup after loading the view.
        
        for i in 0..<SegmentViewController.RealTime.realtimeTabArray.count
        {
            if(SegmentViewController.RealTime.realtimeTabArray[i] == "PEOPLE")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
                getSteps = "PEOPLE"
                break
            }
            else
            {
                self.saveButton.isUserInteractionEnabled = false
                self.commitButton.isUserInteractionEnabled = false
                getSteps = ""
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkIsCommit()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return jsonPeopleData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (playerValueGameVariable[section] as AnyObject).count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.peopleTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PeopleTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.tintColor = UIColor.white
        cell.textField.delegate=self
        let gameVariableName = (playerValueGameVariable[indexPath.section] as AnyObject).value(forKey: "GameVariableName") as! NSArray
        let gameValue = (playerValueGameVariable[indexPath.section] as AnyObject).value(forKey: "Value") as! NSArray
        
        cell.label.text = gameVariableName[indexPath.row] as? String
        cell.textField.text = String(describing: getValue[indexPath.section][indexPath.row])
        
            cell.textField.tag = (indexPath.section * 1000) + indexPath.row
        if(getSteps == "PEOPLE" || simulateValue != "RealTime")
        {
            cell.textField.addTarget(self, action: #selector(self.getText), for: .editingDidEnd)
        }
        else
        {
            cell.textField.isUserInteractionEnabled = false
        }
        cell.textField.keyboardType = .numberPad
        cell.accessoryType = cell.isSelected ? .checkmark : .none
        return cell
    }
    //set tittle for section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let sectionValue = (jsonPeopleData[section] as AnyObject).value(forKey: "LocationName") as! String
        return sectionValue
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    /*!
     *@brief get GameVariable data (people data) from server and reload the tableview to update the data
     */
    func getGameVariableData()
    {
        let url = urlStrings.getPlayerLocationIdandGameVariableId
        let urlValue = URL(string: "\(url)\(5)")!//stepid =5
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print("error")
                }
                else
                {
                    do
                    {
                        self.getValue.removeAll()
                        self.jsonPeopleData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.mutableContainers) as! NSArray
                        print(self.jsonPeopleData)
                        self.playerValueGameVariable = self.jsonPeopleData.value(forKey: "playerValueGameVariable") as! NSArray
                        for i in 0..<self.jsonPeopleData.count
                        {
                            let gameValue = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "Value") as! NSArray
                            var valueArray = [Int]()

                            for j in 0..<(self.playerValueGameVariable[i] as AnyObject).count
                            {
                                valueArray.append(gameValue[j] as! Int)
                                let isSelected = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "IsSelected") as! NSArray
                                let state = (self.playerValueGameVariable[i] as AnyObject).value(forKey: "CheckState") as! NSArray
                                if(isSelected[j]) as! Bool
                                {
                                    //for new user Is selected is 0 this loop is not execute for new user
                                    self.checkIntialState = 1
                                }
                                if(state[j] as! Int != 0)
                                {
                                    self.checkStatusCount = 1;
                                    self.checkStatus = state[j] as! Int
                                }
                            }
                            self.getValue.append(valueArray as [Int])
                        }
                        
                        if(self.checkStatusCount == 0)
                        {
                            self.checkStatus = 0
                        }
                        //In First time(new user) check State is 0.
                        //if checkstatus = 2 save button intraction isenable
                        //set checkStatus =2
                        //now savebutton will be enable at new user
                        if(self.checkIntialState == 0)
                        {
                            self.checkStatus = 2
                        }
                        if(self.checkStatus == 0)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = false
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 2)
                        {
                            DispatchQueue.main.async()
                                {
                                    //set border for enabled button
                                    self.saveButton.layer.borderColor = UIColor.white.cgColor
                                    self.saveButton.layer.borderWidth=1
                                    self.commitButton.layer.borderWidth=0
                                    self.saveButton.isHidden=false
                                    self.saveButton.isUserInteractionEnabled = true
                                    self.commitButton.isUserInteractionEnabled=false
                                    self.commitButton.setTitle("Commit",for: .normal)
                            }
                        }
                        else  if(self.checkStatus == 1)
                        {
                            DispatchQueue.main.async()
                                {
                                    self.commitButton.layer.borderColor = UIColor.white.cgColor
                                    self.commitButton.layer.borderWidth=1
                                    self.saveButton.isHidden=true
                                    self.commitButton.isUserInteractionEnabled=true
                                    self.commitButton.setTitle("Revoke",for: .normal)
                            }
                            
                        }
                        DispatchQueue.main.async(){
                            
                            //check for step read access
                            self.stepCheck()
                            self.peopleTable.reloadData()
                        }
                    }
                    catch let error as NSError
                    {
                        print(error)
                    }
                }
        })
        task.resume()
    }
    @IBAction func saveButton(_ sender: Any) {
        urlString = urlStrings.savePlayerGameVariable
        savePeople()
        //send notification when user in realtime simulation
        if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
        {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" People Saved"])
        }

        
    }
    @IBAction func commitButton(_ sender: Any) {
        if(self.checkStatus == 1)
        {
            urlString = urlStrings.revokePlayerGameVariable
            savePeople()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" People Revoked"])
            }
        }
        else  if(self.checkStatus == 0)
        {
            urlString = urlStrings.submitPlayerGameVariable
            savePeople()
            //send notification when user in realtime simulation
            if(SimulationViewController.SimulateCheckStatus.simulateCheck == "realTime")
            {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Save_Commit_Revoke"), object: nil,userInfo:["Save_Commit_RevokeData" :" People Commited"])
            }
        }
        
    }
    @IBOutlet weak var commitButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    func savePeople()
    {
        var getValue = [Int]()
        var getelement = [String : Any]()
        var peopleJsonarr = [Any]()
        
        
        //get values from each section and row
        //i(section) j(row) store value in array base on section
        for i in 0..<jsonPeopleData.count
        {
            for j in 0..<(playerValueGameVariable[i] as AnyObject).count
            {
                let indexPath = IndexPath(row: j, section: i)
                let cell = peopleTable.cellForRow(at: indexPath) as! PeopleTableViewCell
                let textValue = cell.textField.text!
                getValue.append(Int(textValue)!)
            }
            getValueBySection.append(getValue)
            getValue.removeAll()
        }
        //construct json data from tableview(section and row)
        for i in 0..<jsonPeopleData.count
        {
            for j in 0..<(playerValueGameVariable[i] as AnyObject).count
            {
                let gameVariableid = (playerValueGameVariable[i] as AnyObject).value(forKey: "GameVariableId") as! NSArray
                let gameVariable = ["GameVariableId":gameVariableid[j],"Value":getValueBySection[i][j]] as [String : Any]
                gameVariableIdValue.append(gameVariable as AnyObject)
            }
            //store gamevariablevalue as array by row count
            getelement = ["LocationId":(jsonPeopleData[i] as AnyObject).value(forKey: "locationId")!,"stepId":"5","GameVariableValue":gameVariableIdValue] as [String : Any]
            gameVariableIdValue.removeAll()
            peopleJsonarr.append(getelement)
            
        }
        print(peopleJsonarr)
        
        //send json data to server
        let jsonData = try? JSONSerialization.data(withJSONObject: peopleJsonarr, options: .prettyPrinted)
        let decodedValue = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as! NSArray
        
        let jsonData1 = try? JSONSerialization.data(withJSONObject: decodedValue! , options: .prettyPrinted)
        let urlValue = URL(string: urlString)!
        var request = URLRequest(url: urlValue)
        request.httpMethod="POST"
        request.httpBody=jsonData1
        //set content type
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler:
            {
                (data, response, error)in
                if(error != nil)
                {
                    print(error!)
                }
                else
                {
                    do
                    {
                        //get response from server as String
                        let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                        print(postResponse)
                        if(self.checkStatus == 0)
                        {
                            let userId = UserDefaults.standard
                            let segVal = userId.value(forKey: "segmentVal") as! Int
                            userId.set(segVal+1, forKey: "segmentVal")
                            userId.synchronize()
                            let sendSegmentValue = ["SegmentValue":userId.value(forKey: "segmentVal")]
                            print(sendSegmentValue)
                            NotificationCenter.default.post(name: Notification.Name("RedirectSegment"), object:nil,userInfo:sendSegmentValue)
                        }
                        
                        //after save checkStatus set to 0
                        if(self.checkStatus == 2)
                        {
                            if(postResponse == "true")
                            {
                                self.checkStatus=0
                                self.getGameVariableData()
                            }
                        }
                        else
                        {
                            if(postResponse == "true")
                            {
                                self.getGameVariableData()
                            }
                        }
                        
                    }
                    
                }
                
        })
        
        task.resume()
        
        
    }
    func getText(_ sender: Any) {
        
        let textField: UITextField? = (sender as? UITextField)
        section = (sender as AnyObject).tag/1000
        row = (sender as AnyObject).tag%1000
        let gameBenchMarkValue = (playerValueGameVariable[section] as AnyObject).value(forKey: "BenchmarkValue") as! NSArray
        let value = gameBenchMarkValue[2] as! Int
        let textValue = Int((textField?.text)!)
         getValue[section][row] = textValue!
        print(getValue)
        if(value > textValue!)
        {
            let gamepeople = (playerValueGameVariable[section] as AnyObject).value(forKey: "GameVariableName") as! NSArray
            let sectionValue = (jsonPeopleData[section] as AnyObject).value(forKey: "LocationName") as! String
            let alertController = UIAlertController(title: "\(gamepeople[2]) for \(sectionValue)", message: "Value Should be greater than \(value)", preferredStyle: UIAlertControllerStyle.alert)
            
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            {
                (result : UIAlertAction) -> Void in
                textField?.text = String(value+1)
                
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func hideKeyboard() {
        peopleTable.endEditing(true)
    }
    func checkIsCommit()
    {
        let url = urlStrings.isCommit
        let urlValue = URL(string: "\(url)\(5)")!
        let session = URLSession.shared
        let task = session.dataTask(with: urlValue, completionHandler: {
            (data, response, error)in
            if(error != nil)
            {
                print("error")
            }
            else
            {
                do
                {
                    let postResponse = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)! as NSString
                    print(postResponse)
                    if(postResponse == "true")
                    {
                        self.getGameVariableData()
                    }
                    else
                    {
                        
                    }
                    
                    
                }
            }
        })
        task.resume()
    }
    func segmentChange(notification: Notification)
    {
        self.viewWillAppear(true)
    }
    /*!
     *@brief While enter username and password keyboardDidShow fuction execute(NSNotification).
     */
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidShow), name:NSNotification.Name.UIKeyboardDidShow, object: nil);
        return true
        
    }
    /*!
     *@brief keyboard disappear when click return/done button
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    /*!
     *@brief after enter username and password keyboardDidHide fuction execute(NSNotification).
     */
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.autocorrectionType = .no
        NotificationCenter.default.addObserver(self, selector:#selector(keyboardDidHide), name:NSNotification.Name.UIKeyboardDidHide, object: nil);
        return true
        
        
    }
    /*!
     *@brief hide statusbar
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }
    /*!
     *@brief move view to top when keyboard appears
     */
    func keyboardDidShow(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.peopleTable.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.peopleTable.frame = tvFrame
    }
    /*!
     *@brief set view to orginal height when keyboard hide
     */
    func keyboardDidHide(notification:NSNotification) {
        let initialFrame: CGRect? = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let convertedFrame = self.view.convert(initialFrame!, from: nil)
        // var convertedFrame: CGRect = self.view.convertRect(initialFrame, from: nil)
        var tvFrame: CGRect = self.peopleTable.frame
        tvFrame.size.height = convertedFrame.origin.y
        self.peopleTable.frame = tvFrame
    }
    //check for step read access
    func stepCheck()
    {
        
        if(simulateValue == "RealTime")
        {
            if(getSteps == "PEOPLE")
            {
                self.saveButton.isUserInteractionEnabled = true
                self.commitButton.isUserInteractionEnabled = true
            }
            else
            {
                self.saveButton.isHidden = true
                self.commitButton.isHidden = true
            }
        }
    }
    
}
