//
//  Allocation1TableViewCell.swift
//  skywalker
//
//  Created by Sabari on 3/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class Allocation1TableViewCell: UITableViewCell {

    @IBOutlet weak var gameVariableLabel: UILabel!
    @IBOutlet weak var textFieldValue: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
