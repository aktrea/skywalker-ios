//
//  DashboardViewController.swift
//  skywalker
//
//  Created by Sabari on 14/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var dashBoardData = NSDictionary()
    var teamMember = NSDictionary()
    var teamMemberList = NSArray()
    @IBOutlet var groupName: UILabel!
    @IBOutlet var teamName: UILabel!
    @IBOutlet var playerName: UILabel!
    @IBOutlet var dashboardTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async(){
    let outData = UserDefaults.standard.data(forKey: "dashboard_data")
            self.dashBoardData = NSKeyedUnarchiver.unarchiveObject(with: outData!)! as! NSDictionary
        self.playerName.text = self.dashBoardData.value(forKey: "PlayerName") as! String?
        self.teamName.text = self.dashBoardData.value(forKey: "GameIntoduction") as! String?
        self.teamMember = self.dashBoardData.value(forKey: "TeamMembers") as! NSDictionary
        let value = self.teamMember.value(forKey: "GroupName") as! String
        self.groupName.text = value
            self.teamMemberList = self.teamMember.value(forKey: "TeamUsers") as! NSArray
            print(self.teamMemberList)
            self.dashboardTable.reloadData()
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "loginSuccess")
        {
            DispatchQueue.main.async(){
                _ = segue.destination
            }
        }
    }
    
    @IBAction func CloseButton(_ sender: Any) {
        DispatchQueue.main.async()
            {
                self.performSegue(withIdentifier: "loginSuccess", sender:nil)
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return teamMemberList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dashboardTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath) 
        cell.backgroundColor = UIColor.clear 
        let memberName = cell.contentView.viewWithTag(1) as! UILabel
        let memberRole = cell.contentView.viewWithTag(2) as! UILabel

       
        memberName.text = (teamMemberList[indexPath.row] as AnyObject).value(forKey: "UserName") as? String
        
        memberRole.text = (teamMemberList[indexPath.row] as AnyObject).value(forKey: "PlayerRole") as? String
       
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        
        return "Team Meambers"
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.36)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // This will create a "invisible" footer
        return 0.01
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
   }
