//
//  iPadIntialViewController.swift
//  skywalker
//
//  Created by Sabari on 27/3/17.
//  Copyright © 2017 Sabari. All rights reserved.
//

import UIKit

class iPadIntialViewController: UIViewController {
    var jsonDashBoardData = NSDictionary()
    var teamMember = NSDictionary()
    @IBOutlet var groupName: UILabel!
    @IBOutlet var teanName: UILabel!
    @IBOutlet var playerName: UILabel!
    @IBOutlet var profilePicture: UIImageView!
    
    @IBOutlet var containerView: [UIView]!
    @IBOutlet var viewBut: [UIButton]!
   
    @IBOutlet var pricing: UIView!
    @IBOutlet var financial: UIView!
    @IBOutlet var cost: UIView!
    @IBOutlet var people: UIView!
    @IBOutlet var promotionView: UIView!
    @IBOutlet var channelView: UIView!
    @IBOutlet var productView: UIView!
    @IBOutlet var locationView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePicture.layer.borderColor = UIColor.white.cgColor
        
        profilePicture.layer.borderWidth = 1.5
        let outData = UserDefaults.standard.data(forKey: "dashboard_data")
        jsonDashBoardData = NSKeyedUnarchiver.unarchiveObject(with: outData!)! as! NSDictionary
        self.playerName.text = self.jsonDashBoardData.value(forKey: "PlayerName") as! String?
        self.teanName.text = self.jsonDashBoardData.value(forKey: "GameIntoduction") as! String?
        self.teamMember = self.jsonDashBoardData.value(forKey: "TeamMembers") as! NSDictionary
        let value = self.teamMember.value(forKey: "GroupName") as! String
        self.groupName.text = value
        
        for i in 0..<self.containerView.count
        {
            self.containerView[i].isHidden=true
        }
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func locationBut(_ sender: Any) {
        setSelect(getValue: 0)
    }
    @IBAction func productBut(_ sender: Any) {
        setSelect(getValue: 1)
    }
    @IBAction func channelBut(_ sender: Any) {
        setSelect(getValue: 2)
    }
    @IBAction func promotionBut(_ sender: Any) {
        setSelect(getValue: 3)
    }
    @IBAction func peopleBut(_ sender: Any) {
        setSelect(getValue: 4)
    }
    
    @IBAction func costBut(_ sender: Any) {
        setSelect(getValue: 5)
    }
    @IBAction func financialBut(_ sender: Any) {
        setSelect(getValue: 6)
    }
    @IBAction func priceBut(_ sender: Any) {
        setSelect(getValue: 7)
    }
    func setSelect(getValue : Int)
    {
        
        for i in 0..<viewBut.count
        {
            if(i == getValue)
            {
                viewBut[i].layer.cornerRadius = 5
                viewBut[i].layer.masksToBounds = true
                viewBut[i].backgroundColor = UIColor.white
                viewBut[i].layer.borderColor = UIColor.white.cgColor
                viewBut[i].layer.borderWidth = 2
                viewBut[i].setTitleColor(UIColor.black, for: UIControlState.normal)
//                getView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
//                UIView.animate(withDuration:0.5, animations: {() -> Void in
//                    getView.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//                }, completion: {(_ finished: Bool) -> Void in
//                    print(" Animation complet Block")
//                })
                containerView[i].isHidden = false
            }
            else
            {
                viewBut[i].layer.cornerRadius = 5
//                viewBut[i].layer.masksToBounds = true
                viewBut[i].layer.borderColor = UIColor.white.cgColor
                viewBut[i].layer.borderWidth = 2
                viewBut[i].backgroundColor = UIColor.clear
                viewBut[i].setTitleColor(UIColor.white, for: UIControlState.normal)
                containerView[i].isHidden = true
//                let maskPAth1 = UIBezierPath(roundedRect: self.viewBut[i].bounds,
//                                             byRoundingCorners: [.topLeft , .bottomRight],
//                                             cornerRadii:CGSize(width:20.0, height:20.0))
//                let maskLayer1 = CAShapeLayer()
//                maskLayer1.frame = self.viewBut[i].bounds
//                maskLayer1.path = maskPAth1.cgPath
//                self.viewBut[i].layer.mask = maskLayer1
    @IBAction func scoreBut(_ sender: Any) {
        DispatchQueue.main.async(){
            self.performSegue(withIdentifier: "ScoreView", sender:nil)
            self.performSegue(withIdentifier: "Score", sender:nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "ScoreView")
        if(segue.identifier == "Team")
        {
            DispatchQueue.main.async(){
                _ = segue.destination
            }
        }
        else if(segue.identifier == "Score")
        {
            DispatchQueue.main.async(){
                _ = segue.destination as! ScoreViewController
            }
        }
    }
    @IBAction func TeamBut(_ sender: Any) {
                DispatchQueue.main.async(){
                    self.performSegue(withIdentifier:"Team", sender:nil)
                }
        
    }
}
